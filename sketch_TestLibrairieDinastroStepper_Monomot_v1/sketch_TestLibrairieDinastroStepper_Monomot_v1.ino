
/*
 * sketch_TestLibrairieDinastroStepper_Monomot
 * Version 1 du 5 nov. 2015 - Jean-Paul GARRIGOS pour Dinastro
 * --------------------------------------
 * Gestion du moteur pas à pas pour le projet DinastroDriv
 * Gestion des modes de rattrapages selon vitesses de centrale, pointage et rattrapage
 * Intégre rampe pour vitesse de rattrapage
 * Version prévue pour fonctionner sur la carte Monomot (version à partir de 2.52Pro).
 */

#include <DinastroPad.h>
#include <DinastroStepper_Monomot.h>
#include <FrequencyTimer2.h>	

/*
 * Variables tests
 */
int _currentSelectedSpeed;								// La vitesse courante sélectionnée pour les corrections par l'utilisateur
int _desiredState;										// L'état courant de l'axe
/*
 * Constantes des modes de vitesse de correction
 */
const unsigned int CENTERING = 1; 						// La vitesse courante sélectionnée pour les corrections est le centrage
const unsigned int POINTING = 2;						// La vitesse courante sélectionnée pour les corrections est le pointage
const unsigned int CATCH_UPING = 3;						// La vitesse courante sélectionnée pour les corrections est le rattrapage
/*
 * Constantes Moteur
 */
const int STEPPER_STEP = 400;
const unsigned long TIMER_PERIOD = 100;					// Période du timer2 en micro-secondes
const unsigned long TRACKING_PERIOD = 300000;        	// Période de la vitesse de suivi en micro-secondes
const unsigned long EST_CENTERING_PERIOD = 600000;  	// Période de la vitesse de centrage (est) en micro-secondes (2x - vitesse de suivi)
const unsigned long WEST_CENTERING_PERIOD = 150000;  	// Période de la vitesse de centrage (ouest) en micro-secondes (2x + vitesse de suivi)
const unsigned long POINTING_PERIOD = 75000;     		// Période de la vitesse de pointage (est) en micro-secondes (4x  vitesse de suivi)
const unsigned long CATCH_UP_PERIOD = 1000;          	// Période de la vitesse de rattrapage (deux sens) en micro-secondes
const unsigned long ACCEL_PERIOD = 9000;             	// Période de calcul de la vitesse en phase d'acceleration ou ralentissement en micro-secondes
const unsigned long ACCEL_STEP_PSS = 20000;            	// Le coefficient d'accélération et de ralentissement en Pas par Secondes par Secondes (minimum 200 pas par secondes par secondes)
const unsigned long ACCEL_STEP_OVERCOME = 2;            // Pallier en nombres de pas avant de changer de seuil de vitesse lors des phases d'acceleration ou de ralentissement
/*
 * Déclatation du moteur pas à pas
 */
DinastroStepper stepper(STEPPER_STEP,
							TIMER_PERIOD,
							TRACKING_PERIOD,
							EST_CENTERING_PERIOD,
							WEST_CENTERING_PERIOD,
							POINTING_PERIOD,
							CATCH_UP_PERIOD,
							ACCEL_PERIOD,
							ACCEL_STEP_PSS,
							ACCEL_STEP_OVERCOME);
 

/*
 * Déclaration de la raquette
 */
DinastroPad pad(onLowSpeedButtonPressEvent,
                  onLowSpeedButtonReleaseEvent,
                  onMediumSpeedButtonPressEvent,
                  onMediumSpeedButtonReleaseEvent,
                  onHighSpeedButtonPressEvent,
                  onHighSpeedButtonReleaseEvent,
                  onWestButtonPressEvent,
                  onWestButtonReleaseEvent,
                  onEastButtonPressEvent,
                  onEastButtonReleaseEvent,
                  onSouthButtonPressEvent,
                  onSouthButtonReleaseEvent,
                  onNorthButtonPressEvent,
                  onNorthButtonReleaseEvent
                  );

void setup() {
	
	stepper.disableStepper();
	/*
	 * Initialisation du Timer2
	 */
	FrequencyTimer2::setOnOverflow(onTimer2);      		// Association de l'interruption avec la fonction de traitement de la commutation de l'étage de motorisation (toogleOutput)
	FrequencyTimer2::disable();
	FrequencyTimer2::setPeriod(TIMER_PERIOD/2);
	
	
	_currentSelectedSpeed = CENTERING;					// La vitesse de correction par défaut est le centrage
	_desiredState = DinastroStepper::TRACKING_STATE;	// Le mode par défaut est le suivi
	 
	Serial.begin(9600);
	Serial.println("---------- Starting ...");
	
	stepper.enableStepper();
	FrequencyTimer2::enable();
}

void loop() {
 
	pad.updateStatus();
  
	/*
	 * Machine à états
	 */
	switch (_desiredState)
	{
		case DinastroStepper::TRACKING_STATE:
			stepper.launchTracking();
			break;
		case DinastroStepper::WEST_CENTERING_STATE:
			stepper.launchWestCentering();
			break;
		case DinastroStepper::EST_CENTERING_STATE:
			stepper.launchEstCentering();
			break;
		case DinastroStepper::WEST_POINTING_STATE:
			stepper.launchWestPointing();
			break;
		case DinastroStepper::EST_POINTING_STATE:
			stepper.launchEstPointing();
			break;
		case DinastroStepper::WEST_CATCH_UP_STATE:
			stepper.launchWestCatchUP();
			break;
		case DinastroStepper::EST_CATCH_UP_STATE:
			stepper.launchEstCatchUP();
			break;
		case DinastroStepper::RETURNING_TO_TRACKING_STATE:
			stepper.launchReturnToTracking();
			break;
	}
	/*
	 * A la fin d'une phase de retour vers la vitesse de suivi, il faut mettre à jour la vitesse désirée à la vitesse 
	 * de suivi. Car le moteur passe automatiquement dans ce mode.
	 */
	if ( (stepper.getState() == DinastroStepper::TRACKING_STATE) && (_desiredState == DinastroStepper::RETURNING_TO_TRACKING_STATE) )
	{
		_desiredState = DinastroStepper::TRACKING_STATE;
	}
 
    Serial.print("Position : ");
    Serial.print(stepper.getPosition(),DEC);
    Serial.print(" Direction : ");
    Serial.print(stepper.getDirection(),DEC);
    Serial.print(" Mode Moteur : ");
    Serial.print(stepper.getStepperMode(),DEC);
    Serial.print(" Etat Moteur : ");
    Serial.print(stepper.getState(),DEC);
	Serial.print(" Vitesse selectionnee : ");
    Serial.print(_currentSelectedSpeed,DEC);
    Serial.print(" Etat a lancer : ");
    Serial.println(_desiredState,DEC);
    
}

/*
 * Déclaration de la procédure ISR du Timer2
 */
void onTimer2()
{
	stepper.toogleOutput();
}

/*
 *  Déclaration des procédures de gestions des événements des boutons de la raquette
 */
void onLowSpeedButtonPressEvent()
{
  Serial.println("Low-Speed ON !");
}
void onLowSpeedButtonReleaseEvent()
{
  Serial.println("Low-Speed RELEASE !");
  _currentSelectedSpeed = CENTERING;
}
void onMediumSpeedButtonPressEvent()
{
  Serial.println("Medium-Speed ON !");
}
void onMediumSpeedButtonReleaseEvent()
{
  Serial.println("Medium-Speed RELEASE !");
  _currentSelectedSpeed = POINTING;
}
void onHighSpeedButtonPressEvent()
{
  Serial.println("High-Speed ON !");
}
void onHighSpeedButtonReleaseEvent()
{
  Serial.println("High-Speed RELEASE !");
  _currentSelectedSpeed = CATCH_UPING;
}
void onWestButtonPressEvent()
{
  Serial.println("West ON !");
  switch (_currentSelectedSpeed)
  {
	  case CENTERING:
		_desiredState = DinastroStepper::WEST_CENTERING_STATE;
		break;
	  case POINTING:
		_desiredState = DinastroStepper::WEST_POINTING_STATE;
		break;
	  case CATCH_UPING:
		_desiredState = DinastroStepper::WEST_CATCH_UP_STATE;
		break;
  }
}
void onWestButtonReleaseEvent()
{
  Serial.println("West RELEASE !");
  switch (_currentSelectedSpeed)
  {
	  case CENTERING:
		_desiredState = DinastroStepper::TRACKING_STATE;
		break;
	  case POINTING:
		_desiredState = DinastroStepper::TRACKING_STATE;
		break;
	  case CATCH_UPING:
		_desiredState = DinastroStepper::RETURNING_TO_TRACKING_STATE;
		break;
  }
}
void onEastButtonPressEvent()
{
  Serial.println("East ON !");
  switch (_currentSelectedSpeed)
  {
	  case CENTERING:
		_desiredState = DinastroStepper::EST_CENTERING_STATE;
		break;
	  case POINTING:
		_desiredState = DinastroStepper::EST_POINTING_STATE;
		break;
	  case CATCH_UPING:
		_desiredState = DinastroStepper::EST_CATCH_UP_STATE;
		break;
  }
}
void onEastButtonReleaseEvent()
{
  Serial.println("East RELEASE !");
  switch (_currentSelectedSpeed)
  {
	  case CENTERING:
		_desiredState = DinastroStepper::TRACKING_STATE;
		break;
	  case POINTING:
		_desiredState = DinastroStepper::TRACKING_STATE;
		break;
	  case CATCH_UPING:
		_desiredState = DinastroStepper::RETURNING_TO_TRACKING_STATE;
		break;
  }
}
void onSouthButtonPressEvent()
{
  Serial.println("South ON !");
}
void onSouthButtonReleaseEvent()
{
  Serial.println("South RELEASE !");
}
void onNorthButtonPressEvent()
{
  Serial.println("North ON !");
}
void onNorthButtonReleaseEvent()
{
  Serial.println("North RELEASE !");
}
