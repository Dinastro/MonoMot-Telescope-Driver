/**
 * @file
 * @author  Jean-Paul "JPeGfr" Garrigos <jpegfr@gmail.com>
 * @version 0.8
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * https://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * Le programme monomot est le logiciel firmeware de la carte monomot développé par l'association Dinastro.
 * Elle permet le pilotage d'un axe de télescope astronomique. Elle fait partie du projet DinastroDriv.
 * https://framagit.org/Dinastro/MonoMot-Telescope-Driver
 *
 * IMPORTANT : Cette version est identique pour l'axe ALPHA et l'axe DELTA. Pensez à modifier la valeur axis ci dessous !
 *
 */

#include <DinastroPad.h>
#include <AccelStepper.h>
#include <FrequencyTimer2.h>
#include <TimerOne.h>
/*
 * Constantes générales
 */
static const char VERSION[5] = {'0','.','8','a','\0'};													// Alias pour désigner le numéro de version du firmeware (s'affiche sur la console série au reset de la carte monomot)
static const int CCP1 = 9;																											// Alias pour la sortie de la carte UNO (base de la carte monomot) correspondant au signal CCP1 de l'étage logique
static const int CCP2 = 10;																											// Alias pour la sortie de la carte UNO (base de la carte monomot) correspondant au signal CCP2 de l'étage logique
static const int CNT1 = 14;																											// Alias pour la sortie de la carte UNO (base de la carte monomot) correspondant au signal CNT1 de l'étage logique
static const int CNT2 = 15;																											// Alias pour la sortie de la carte UNO (base de la carte monomot) correspondant au signal CNT2 de l'étage logique
static const int SCREW = 8;																											// Alias pour l'entrée de la carte UNO (base de la carte monomot) correspondant au connecteur SV5 "retour de vis" (monomot > 2.52)
static const int HIGHIMPEDANCE = A3;																						// Alias pour la sortie de la carte UNO (base de la carte monomot) AD3 (17) soit la broche qui permet de sortir le moteur du mode haute impédance (monomot > 2.52)
static const int TRACKINGMODEINPUT = 7;																					// Alias pour l'entrée de la carte UNO (base de la carte monomot) IO7 (7) soit la broche qui permet de receuillir le mode de vitesse de suivi (si à 1 logique le mode lunaire est actif). On utilise une broche du connecteur d'extension (monomot > 2.52)
static const int ALPHA = 0; 																										// Alias pour définir un axe à motoriser de type ALPHA
static const int DELTA = 1; 																										// Alias pour définir un axe à motoriser de type DELTA
static const int PV = 0;																												// Alias pour définir le mode de rattrapage en Petite Vitesse
static const int MV = 1;																												// Alias pour définir le mode de rattrapage en Vitesse Moyenne
static const int GV = 2;																												// Alias pour définir le mode de rattrapage en Grande Vitesse
static const int STOP = 0;																											// Alias pour le mode arrêt (type de pas)
static const int MICRO = 1;																											// Alias pour le mode Micro Pas (type de pas)
static const int FULL = 2;																											// Alias pour le mode Pas entier (type de pas)
static const int ON = 1;																												// Alias pour ON
static const int OFF = 0;																												// Alias pour OFF
static const int CWDIRECTION = 1;																								// Alias pour le sens de rotation vers l'Ouest ou le Sud (sens des aiguilles d'une montre)
static const int CCWDIRECTION = 2;																							// Alias pour le sens de rotation vers l'Est ou le Nord (sens inverse des aiguilles d'une montre)
static const int STELLARTRACKING = 0;																						// Alias pour le mode de suivi à la vitesse sidérale (vitesse 15.041 arc/sec voir constante STELLARTRACKINGPERIOD)
static const int MOONTRACKING = 1;																							// Alias pour le mode de suivi à la vitesse lunaire (vitesse 14.492 arc/sec voir constante MOONTRACKINGPERIOD)
/*
 * Constantes spécifiques à la motorisation et à l'instrument utilisé
 */
static const int STELLARTRACKINGPERIOD = 3750;																	// Période à appliquer au Timer 2 correspondant à la vitesse de suivi sidérale (vitesse 15.041 arc/sec)
static const int MOONTRACKINGPERIOD = 3886; 																		// Période à appliquer au Timer 2 correspondant à la vitesse de suivi lunaire (vitesse 14.492 arc/sec)
static const int PVALPHAEDELTANSPERIOD = 2000;																	// Période à appliquer au Timer 2 correspondant à la vitesse de rattrapage PV sens Est en ALPHA ou rattrapage PV dans les deux sens en DELTA
static const int PVALPHAWPERIOD = 4500;																					// Période à appliquer au Timer 2 correspondant à la vitesse de rattrapage PV sens Ouest en ALPHA
static const int MVGVPERIOD = 100;																							// Période à appliquer au Timer 2 correspondant aux vitesses en pas entiers MV et GV (n'a pas d'impact sur les vitesses MV ni GV mais doit être suffisant courte, par défaut 100)
static const int MVSPEED = 30;																									// Vitesse de rotation constante en pas par seconde (exemple 2000 = 10 tours/s) pour la vitesse MV
static const int GVSPEED = 600;																									// Vitesse de rotation constante en pas par seconde (exemple 2000 = 10 tours/s) pour la vitesse GV
static const int SCREWBACKSPEED = 600;																					// Vitesse de rotation constante en pas par seconde (exemple 2000 = 10 tours/s) pour la vitesse de la fonction "retour de vis"
static const long STEPSPERAXISMAXNUMBER = 573852;																// Nombre maximal de pas par tour d'axe du telescope (ce nombre de pas correspond au produit du nombre de pas par tour du moteur et du rapport de transmission installé sur l'axe)
static const int ACCELERATIONCOEF = 300;																				// Le coefficient d'accélération du moteur en nombre de pas par secondes par secondes (voir librairie AccelStepper http://www.airspayce.com/mikem/arduino/AccelStepper/classAccelStepper.html#adfb19e3cd2a028a1fe78131787604fd1) pour la gestion des rampes en mode pas entiers
static const int BEFORESTEPSTOPNUMBER = 1;																			// Le nombre de pas entier(s) avant de lancer le suivi en ALPHA après une rampe
//int sinTable[8]={400,500,600,700,800,850,950,1000}; 														// Table des sinus en 8éme de pas
int sinTable[8]={100,200,300,400,500,650,850,1000}; 														// Table des sinus en 8éme de pas
/*
 * Initialisation des variables et des objets
 */
AccelStepper stepper(2,CNT1,CNT2);																							// Constructeur du mode pas entier à partir de la librairie AccelStepper en mode 2 fils (broches CNT1 et CNT2 de l'étage logique)
unsigned char inPins[4] = { 0b00001011,
								0b00001001,
								0b00001000,
								0b00001010};																										// Séquences des validations de l'étage logique (Arduino/AVR/Etage-logique : AD0/PC0/CNT1 et AD1/PC1/CNT2)
																																								// On utilise également la broche AD3/PC3/HIGHIMPEDANCE, qui permet de placer le moteur en haute impédance ou non
int FrequencyMultiplicator = 10;																								// Un multiplicateur pour la gestion de la fréquence moteur (10 par défaut)
int FrequencyCounter = 0;																												// Compteur utilisé pour la boucle de gestion de la fréquence moteur
int inPinCounter = 3;																														// Compteur utilisé pour parcourir le tableau des séquences des validations de l'étage logique inPins[] (condition initiale inPinCounter=3)
int PWMCounter = 0;																															// Compteur utilisé pour parcourir le tableau des valeurs des PWM (table des sinus sinTable[])
int cycle = 1;																																	// Pour déterminer dans quel cycle moteur on se trouve (voir toogleOutput(), condition initiale cycle=1)
int _stepperMode = STOP;																							// initialisation en mode micro pas (0: arret; 1 micro; 2 pas entiers)
int _axisState = STOP;																													// état de la motorisation de l'axe (0 arrêt total; 1 mode déplacement; 2 mode pas entier ; 3 mode vitesse de suivi en micro pas, etc ... cf fonction changeAxisState() )
int _catchingSpeed = PV;																												// Vitesse rattrappage (PV = 0 ; MV = 1 ; GV = 2 ; STOP = 3)
int _newCatchingSpeed = PV;																											// Idem mais est utilisé pour retarder l'application de la vitesse de consigne lorsque on se trouve dans la rampe de ralentissement
int _direction = CWDIRECTION;																										// Le sens de rotation du moteur (par défaut dans le sens des aiguilles d'une montre)
int _trackingMode = STELLARTRACKING;																						// Le mode appliqué pour la vitesse de suivi (STELLARTRACKING:0; MOONTRACKING:1 respectivements sidérale et lunaire et par défaut sidérale)
long _targetPos = 0;																														// La position souhaitée pour le mode de fonctionnement en positionnement (la valeur est modifié depuis l'I2C)
boolean _debug = true;																													// Par défaut le mode d'affichage des traces exécutives est désactivé
/*
 * Définition du type d'axe à motoriser
 */
int _axis = ALPHA;																															// Constante à modifier selon le type d'axe à motoriser utiliser les alias ALPHA ou DELTA (0 : ALPHA ou 1 : DELTA)
//int _axis = DELTA;

/*
 * Déclaration des procédures de gestions des événements des boutons de la raquette
 */
void onLowSpeedButtonPressEvent()
{
	/*
	 * Evénement "Appui sur le bouton Petite Vitesse"
	 */
	 if ( _debug )
	 {
		 Serial.println("Low Speed ON !");
	 }
}
void onLowSpeedButtonReleaseEvent()
{
	/*
	 * Evénement "Relachement du bouton Petite Vitesse"
	 */
	 setCatchingSpeed(PV);																												// On configure la vitesse de rattrapage de l'axe en PV
	 if ( _debug )
	 {
		 Serial.println("Low Speed RELEASE !");
	 }
}
void onMediumSpeedButtonPressEvent()
{
	/*
	 * Evénement "Appui sur le bouton Moyenne Vitesse"
	 */
	 if ( _debug )
	 {
		 Serial.println("Medium-Speed ON !");
	 }
}
void onMediumSpeedButtonReleaseEvent()
{
	/*
	 * Evénement "Relachement du bouton Moyenne Vitesse"
	 */
	 setCatchingSpeed(MV);																												// On configure la vitesse de rattrapage de l'axe en MV
	 if ( _debug )
	 {
		 Serial.println("Medium-Speed RELEASE !");
	 }
}
void onHighSpeedButtonPressEvent()
{
	/*
	 * Evénement "Appui sur le bouton Grande Vitesse"
	 */
	 if ( _debug )
	 {
		 Serial.println("High-Speed ON !");
	 }
}
void onHighSpeedButtonReleaseEvent()
{
	/*
	 * Evénement "Relachement du bouton Grande Vitesse"
	 */
	 setCatchingSpeed(GV);																												// On configure la vitesse de rattrapage de l'axe en GV
	 if ( _debug )
	 {
		 Serial.println("High-Speed RELEASE !");
	 }
}
void onWestButtonPressEvent()
{
	/*
	 * Evénement "Appui sur le bouton Ouest"
	 */
	 if (getAxisType() == ALPHA)																									// La fonction n'est active que dans le cas d'un axe ALPHA
	 {
		 onWestPress();
	 }
	 if ( _debug )
	 {
		 Serial.println("West ON !");
	 }
}
void onWestButtonReleaseEvent()
{
	/*
	 * Evénement "Relachement du bouton Ouest"
	 */
	 if (getAxisType() == ALPHA) 																									// La fonction n'est active que dans le cas d'un axe ALPHA
	 {
		 onPadReleased();
	 }
	 if ( _debug )
	 {
		 Serial.println("West RELEASE !");
	 }
}
void onEastButtonPressEvent()
{
	/*
	 * Evénement "Appui sur le bouton Est"
	 */
	 if (getAxisType() == ALPHA)																									// La fonction n'est active que dans le cas d'un axe ALPHA
	 {
		 onEastPress();
	 }
	 if ( _debug )
	 {
		 Serial.println("East ON !");
	 }
}
void onEastButtonReleaseEvent()
{
	/*
	 * Evénement "Relachement du bouton Est"
	 */
	 if (getAxisType() == ALPHA)																									// La fonction n'est active que dans le cas d'un axe ALPHA
	 {
		 onPadReleased();
	 }
	 if ( _debug )
	 {
		 Serial.println("East RELEASE !");
	 }
}
void onSouthButtonPressEvent()
{
	/*
	 * Evénement "Appui sur le bouton Sud"
	 */
	 if (getAxisType() == DELTA)																									// La fonction n'est active que dans le cas d'un axe DELTA
	 {
		 onSouthPress();
	 }
	 if ( _debug )
	 {
		 Serial.println("South ON !");
	 }
}
void onSouthButtonReleaseEvent()
{
	/*
	 * Evénement "Relachement du bouton Sud"
	 */
	 if (getAxisType() == DELTA)																									// La fonction n'est active que dans le cas d'un axe DELTA
	 {
		 onPadReleased();
	 }
	 if ( _debug )
	 {
		 Serial.println("South RELEASE !");
	 }
}
void onNorthButtonPressEvent()
{
	/*
	 * Evénement "Appui sur le bouton Nord"
	 */
	 if (getAxisType() == DELTA) 																									// La fonction n'est active que dans le cas d'un axe DELTA
	 {
		 onNorthPress();
	 }
	 if ( _debug )
	 {
		 Serial.println("North ON !");
	 }
}
void onNorthButtonReleaseEvent()
{
  /*
	 * Evénement "Relachement du bouton Nord"
	 */
	 if (getAxisType() == DELTA) 																									// La fonction n'est active que dans le cas d'un axe DELTA
	 {
		 onPadReleased();
	 }
	 if ( _debug )
	 {
		 Serial.println("North RELEASE !");
	 }
}
/*
 * Déclaration de la raquette et association des événements des boutons
 */
 DinastroPad pad(onLowSpeedButtonPressEvent,
                  onLowSpeedButtonReleaseEvent,
                  onMediumSpeedButtonPressEvent,
                  onMediumSpeedButtonReleaseEvent,
                  onHighSpeedButtonPressEvent,
                  onHighSpeedButtonReleaseEvent,
                  onWestButtonPressEvent,
                  onWestButtonReleaseEvent,
                  onEastButtonPressEvent,
                  onEastButtonReleaseEvent,
                  onSouthButtonPressEvent,
                  onSouthButtonReleaseEvent,
                  onNorthButtonPressEvent,
                  onNorthButtonReleaseEvent
                  );

void disableStepper(void)
{
	/*
	 * Permet de placer le moteur pas à pas en mode haute impédance ce qui coupe le courant dans les bobines
	 * Stop également le timer2 qui appel périodiquement la fonction de gestion du moteur toogleOutput()
	 */
	 FrequencyTimer2::disable();
	 digitalWrite(HIGHIMPEDANCE, LOW);
}
void enableStepper(void)
{
	/*
	 * Active l'étage de puissance du L298, ce qui permet de faire circuler le courant dans les bobines du moteur pas à pas
	 * Puis active le timer2 qui appel périodiquement la fonction de gestion du moteur toogleOutput()
	 */
	 digitalWrite(HIGHIMPEDANCE, HIGH);
	 FrequencyTimer2::enable();
}
void setStepperUpdatePeriod(int period)
{
	/*
	 * Permet de mettre à jour la période du Timer 2 qui appel périodiquement la fonction de gestion du moteur toogleOutput()
	 */
	 FrequencyTimer2::setPeriod(period);
}
void setStepperMode(int mode)
{
	/*
	 * Modifie le mode de fonctionnement du moteur pas à pas
	 * (0: arret; 1 micro; 2 pas entiers)
	 */
	 _stepperMode = mode;
	 if ( mode == FULL)
	 {
		 /*
		  * Traitements dans le cas du mode en pas entiers
		  * on place les 2 PWM à 100% : +VCC
			*/
			Timer1.pwm(CCP1, 1024);																										// pwm sur l'entrée CCP1 de l'étage logique
			Timer1.pwm(CCP2, 1024);																										// pwm sur l'entrée CCP2 de l'étage logique
	 }
}
int getStepperMode()
{
	/*
	 * Renvoie le mode de fonctionnement du moteur pas à pas
	 * (0: arret; 1 micro; 2 pas entiers)
	 */
	 return _stepperMode;
}
void setDirection(int direction)
{
	/*
	 * Change le sens de rotation du moteur
	 */
	 _direction = direction;
}
int getDirection()
{
	/*
	 * Retourne le sens de rotation du moteur
	 */
	 return _direction;
}
void setTrackingMode(int mode)
{
	/*
	 * Change le mode de suivi (sidéral ou lunaire)
	 */
	 _trackingMode = mode;
}
int getTrackingMode()
{
	/*
	 * Retourne le mode de suivi (sidéral ou lunaire)
	 */
	 return _trackingMode;
}

int getAxisType()
{
	/*
	 * Retourne le type d'axe motorisé (ALPHA ou DELTA)
	 */
	 return _axis;
}
void toogleOutput(void)
{
	/*
	 * Routine d'interruption du comparateur du Timer 2 - Gestion du moteur
	 * Toutes les n ms on parcours la table des sinus et on bascule les sorties correspondants aux broches in1 à 4 du L298
	 *
	 * Pour tenir compte de la limite à 33 ms maximum du timer 2 on va attendre d'atteindre la valeur 10 du compteur FrequencyCounter
	 * afin de pouvoir basculer les sorties selon une période supérieure à 33 ms.
	 */
	 if ( getStepperMode() == MICRO )
	 {
		 /*
		  * Traitements dans le cas du mode micro-pas
			*/
			FrequencyCounter++;
			if ( FrequencyCounter > ( FrequencyMultiplicator-1 ) )
			{
				FrequencyCounter = 0;																										// remise à zéro du compteur pour un nouveau cycle
			 /*
			  * Traitement du compteur des PWM selon le sens de rotation et le cycle de rampe
			  * Voir documentation du mode micro-pas
			  */
			 	if ((cycle == 0 && getDirection() == CWDIRECTION) || (cycle == 1 && getDirection() == CCWDIRECTION)) PWMCounter++;
			 	if ((cycle == 1 && getDirection() == CWDIRECTION) || (cycle == 0 && getDirection() == CCWDIRECTION)) PWMCounter--;
			 /*
			 	* Traitement des bornes du compteur des PWM
			 	*/
				if (PWMCounter < 0 && cycle == 1)																				// (A) voir documentation du mode micro-pas
				{
					cycle = 0;
					PWMCounter = 0;
					inPinCounter++;
					//if ( getDirection() == CWDIRECTION ) stepper.setCurrentPosition( stepper.currentPosition() + 1 );
				}
				if (PWMCounter < 0 && cycle == 0)																				// (B) voir documentation du mode micro-pas
				{
					cycle = 1;
					PWMCounter = 0;
					inPinCounter--;
				}
				if (PWMCounter > 7 && cycle == 0)																				// (C) voir documentation du mode micro-pas
				{
					cycle = 1;
					PWMCounter = 7;
					inPinCounter++;
				}
				if (PWMCounter > 7 && cycle == 1)																				// (D) voir documentation du mode micro-pas
				{
					cycle = 0;
					PWMCounter = 7;
					inPinCounter--;
					//if ( getDirection() == CCWDIRECTION ) stepper.setCurrentPosition( stepper.currentPosition() - 1 );
				}
			 /*
			  * Traitement des bornes du compteur des broches CNT1 et CNT2
				*/
				if (inPinCounter > 3) inPinCounter = 0;
				if (inPinCounter < 0) inPinCounter = 3;
			 /*
			  * Application des consignes sur les broches de l'étage logique
				*/
				Timer1.pwm(CCP1, sinTable[PWMCounter]);																	// pwm sur l'entrée CCP1 de l'étage logique
				Timer1.pwm(CCP2, sinTable[7-PWMCounter]);																// pwm sur l'entrée CCP2 de l'étage logique
				PORTC = inPins[inPinCounter] & B00001011;																// On applique les valeurs du tableau inPins aux sorties du PortC de l'AVR (entrées CNT1 et CNT2 de l'étage logique)
																																								// Le masque B00000011 permet de ne modifier que les derniers bits du portC
     }
   }
	 if ( getStepperMode() == FULL )
	 {
		 /*
		  * Traitements dans le cas du mode en pas entiers
			* Remarque, les 2 PWM ont été placé à 100% : +VCC dans l'appel de la méthode setStepperMode()
			* En vitesse de rattrapage GV, on applique des rampes d'accelération et de ralentissement
			* Tant que la distance à parcourir est supérieure à BEFORESTEPSTOPNUMBER on applique la rampe.
			* Dans le cas contraire, on passe en vitesse de suivi en ALPHA. On arrête en DELTA.
			*/
			if ( getCatchingSpeed() == GV || getAxisState() == 11 || getAxisState() == 12 )
			{
				/*
				 * En vitesse GV on utilise la fonction de gestion des rampes de AccelStepper
				 */
				 if ( !stepper.run() )
				 {
					 /*
					  * Un arret de la rampe d'acceleration a été demandé ou la position a été atteinte
					  */
						if ( !aNewCatchingSpeedIsDesired() )
						{
							/*
						   * Dans le cas où une nouvelle consigne de vitesse a été demandé, on retarde l'application de la consigne tant que la rampe de ralentissement est en cours.
					 		 * On met donc à jour la consigne modifiée ici, puisque la rampe de ralentissement n'est plus en cours.
					 		 */
							 _catchingSpeed = _newCatchingSpeed;
						}
						if ( getAxisType() == ALPHA )
						{
							/*
							 * cas d'un axe ALPHA : Lancement vitesse suivi
							 */
							 changeAxisState(3);									   													// Vitesse de suivi ALPHA (cf changeAxisState())
						}
						if ( getAxisType() == DELTA ) 						 													// cas d'un axe DELTA : Arret moteur
						{
							/*
							 * cas d'un axe DELTA : Arrêt du moteur
							 */
							 changeAxisState(0);										 													// Arrêt du moteur (cf changeAxisState())
						}
 				 }
			}
			if ( getCatchingSpeed() != GV )
			{
				/*
				 * Pour les autres vitesses de rattrapage on utilise la fonction de rotation en vitesse constante de AccelStepper
				 */
				 stepper.runSpeed();																										// Rotation en vitesse constante (voir librairie AccelStepper)
			}
  }
}
int getAxisState()
{
	/*
	 * Retour l'état actuel de l'axe motorisé
	 */
	 return _axisState;
}
void moveTo(long absoluteTargetPos)
{
	// TODO : Affecter la valeur de positionnement depuis l'I2C
	/*
	 * Affecte la valeur de la position absolue visée souhaitée puis lance le mode moteur adéquate
	 */
	 _targetPos = absoluteTargetPos;
	 changeAxisState(1);																													// Activation du mode de positionnement (cf changeAxisState())
}
long getCurrentPosition()
{
	/*
	 * Retourne la position courante sur l'axe en nombre de pas Moteur
	 */
	 return stepper.currentPosition();
}
void changeAxisState(int mode)
{
	/*
	 * Implémentation des modes de fonctionnement du moteur de l'axe
	 * Configure les paramètres du moteur selon le mode de fonctionnement souhaité
	 * Rappel des alias :
	 * Axe :
	 * ALPHA = 0
	 * DELTA = 1
	 * Modes de rattrapage :
	 * PV = 0
	 * MV = 1
	 * GV = 2
	 * STOP = 0
	 * Mode de fréquences en mode pas à pas :
	 * STOP = 0
	 * MICRO = 1
	 * FULL = 2
	 * Mode déplacement en cours :
	 * ON = 1
	 * OFF = 0
	 * Voir les différents mode de motorisation dans les commentaires de code associés à chaque cas
	 */
	/*
   * Mémorisation de l'état
   */
	 _axisState = mode;
	 if ( _debug )
	 {
		 Serial.print("Changement de l'état moteur vers : ");
		 Serial.println(mode);
	 }
	/*
	 * Traitements selon l'état moteur à appliquer
	 */
	 if ( mode == 0 )
	 {
		 /*
		  * Mode 0 : arret total ou rattrapage MV Est en Alpha
			*/
		 	disableStepper();
		 	setStepperMode(STOP);
	 }
	 if ( mode == 1 )
	 {
		 /*
		  * Mode 1 : positionnement (la valeur _targetPos est recue via l'I2C cf moveTo())
			*/
			disableStepper();
			setStepperMode(FULL);
			setStepperUpdatePeriod(MVGVPERIOD);
			stepper.moveTo(_targetPos);
			enableStepper();
	 }
	 if ( mode == 2 )
	 {
		 /*
		  * Mode 2 : vitesse constante en mode pas entier sens + (rattrapage MV sens Ouest/Sud)
			*/
		  disableStepper();
		  setStepperMode(FULL);
		  setStepperUpdatePeriod(MVGVPERIOD);

		  stepper.setSpeed(MVSPEED);

		  setDirection(CWDIRECTION);
		  enableStepper();
	}
	if ( mode == 3 )
	{
		/*
		 * Mode 3 : vitesse constante en mode micro pas sens + (vitesse de suivi Alpha)
		 */
		 disableStepper();
		 setStepperMode(MICRO);
		 switch (getTrackingMode())
		 {
			 case STELLARTRACKING :
			 /*
				* Application du mode pour la vitesse de suivi sidérale
				*/
				changeMicroStepMode(0);
				break;
			case MOONTRACKING :
			 /*
				* Application du mode pour la vitesse de suivi lunaire
				*/
				changeMicroStepMode(3);
				break;
			default :
			 /*
				* Application du mode pour la vitesse par défaut (suivi sidéral)
				*/
				changeMicroStepMode(0);
				break;
			}
			setDirection(CWDIRECTION);
			enableStepper();
	}
	if ( mode == 4 )
	{
		/*
		 * Mode 4 : vitesse constante rapide en mode pas entier sens + (rattrapage GV sens Ouest/Sud)
		 */
		 disableStepper();
		 setStepperMode(FULL);
		 setStepperUpdatePeriod(MVGVPERIOD);
		 stepper.move(STEPSPERAXISMAXNUMBER);
		 setDirection(CWDIRECTION);
		 enableStepper();
	}
	if ( mode == 5 )
	{
		/*
		 * Mode 5 : Vitesse constante en mode micro pas lente sens + (rattrapage PV sens Est)
		 */
		 disableStepper();
		 changeMicroStepMode(1);
		 setStepperMode(MICRO);
		 setDirection(CWDIRECTION);
		 enableStepper();
	}
	if ( mode == 6 )
	{
		/*
		 * Mode 6 : Vitesse constante en mode micro pas rapide sens + (rattrapage PV sens Ouest)
		 */
		 disableStepper();
		 changeMicroStepMode(2);
		 setStepperMode(MICRO);
		 setDirection(CWDIRECTION);
		 enableStepper();
	}
	if ( mode == 7 )
	{
		/*
		 * Mode 7 : vitesse constante rapide en mode pas entier sens - (rattrapage GV sens Est/Nord)
		 */
		 disableStepper();
		 setStepperMode(FULL);
		 setStepperUpdatePeriod(MVGVPERIOD);
		 stepper.move(-1*STEPSPERAXISMAXNUMBER);
		 setDirection(CCWDIRECTION);
		 enableStepper();
	}
	if ( mode == 8 )
	{
		/*
		 * Mode 8 : vitesse constante en mode pas entier sens + (rattrapage MV sens Nord)
		 */
		 disableStepper();
		 setStepperMode(FULL);
		 setStepperUpdatePeriod(MVGVPERIOD);

		 stepper.setSpeed(-MVSPEED);

		 setDirection(CCWDIRECTION);
		 enableStepper();
	}
	if ( mode == 9 )
	{
		/*
		 * Mode 9 : Vitesse constante en mode micro pas lente sens - (rattrapage PV sens Nord)
		 */
		 disableStepper();
		 changeMicroStepMode(1);
		 setStepperMode(MICRO);
		 setDirection(CCWDIRECTION);
		 enableStepper();
	}
	if ( mode == 10 )
	{
		/*
		 * Mode 10 : Vitesse constante en mode micro pas lente sens + (rattrapage PV sens Sud)
		 */
		 disableStepper();
		 changeMicroStepMode(1);
		 setStepperMode(MICRO);
		 setDirection(CWDIRECTION);
		 enableStepper();
	}
	if ( mode == 11 )
	{
		/*
		 * Mode 11 : vitesse constante en mode pas entier sens - (Retour de vis sens Est)
		 */
		 disableStepper();
		 setStepperMode(FULL);
		 setStepperUpdatePeriod(MVGVPERIOD);
		 stepper.move(-1*STEPSPERAXISMAXNUMBER);
		 setDirection(CCWDIRECTION);
		 enableStepper();
	}
	if (mode == 12)
	{
		/*
		 * Mode 12 : rampe de ralentissement en mode pas entier et vitesse GV après un relachement de bouton directionnel ou l'atteinte du fin de course de la fonction "retour vis"
		 */
		 stepper.stop();
	}
}
void changeMicroStepMode(int mode)
{
	/*
	 * Gestion du mode de rattrapage en mode micro pas
	 *
	 * interruption toutes les 7500 micro secondes soit 7.5 ms avec le timer 2 (pour la vitesse de suivi sidérale)
	 * on utilisera cependant un multiplicateur par 10 supplémentaire (sous la forme d'un compteur : FrequencyCounter) pour arriver à une
	 * période de 75 ms (valeur théorique et vérifiée par chronométrage).
	 *
	 * Mode de Fréquence en mode micro pas (0 = sidéral; 1 = rattrapage PV alpha +, delta +/- ; 2 : rattrapage PV alpha - ; 3 = lunaire)
	 */
	 switch (mode)
	 {
		 case 0 :
		 	/*
			 * Application de la Période pour la vitesse de suivi sidérale
			 */
			 setStepperUpdatePeriod(STELLARTRACKINGPERIOD);
			 break;
		 case 1 :
		 	/*
			 * Application de la Période pour la vitesse de rattrapage PV sens Est en ALPHA ou rattrapage PV dans les deux sens en DELTA
			 */
			 setStepperUpdatePeriod(PVALPHAEDELTANSPERIOD);
			 break;
		 case 2 :
			 /*
			  * Application de la Période pour la vitesse de rattrapage PV sens Ouest en ALPHA
				*/
				setStepperUpdatePeriod(PVALPHAWPERIOD);
				break;
		 case 3 :
			 /*
			  * Application de la Période pour la vitesse de suivi lunaire
				*/
				setStepperUpdatePeriod(MOONTRACKINGPERIOD);
				break;
	 }
}

void setCatchingSpeed(int catchingSpeed)
{
	/*
	 * Permet de modifier le type de vitesse de rattrapage à appliquer sur l'axe
	 * On ne change le type de vitesse que si la rampe de ralentissement n'est pas en cours.
	 * Si c'est le cas, on mémorise la nouvelle consigne de vitesse, que l'on appliquera dés que la rampe est achevée.
	 */
	 if (getAxisState() == 12)
	 {
		 _newCatchingSpeed = catchingSpeed;
	 }
	 else
	 {
		 _newCatchingSpeed = catchingSpeed;
		 _catchingSpeed = catchingSpeed;
	 }
}

boolean aNewCatchingSpeedIsDesired()
{
	/*
	 * Si une nouvelle consigne de vitesse est demandée par l'utilisateur, la méthode recoit Vrai. Faux dans le cas contraire.
	 */
	 return ( _newCatchingSpeed == _catchingSpeed );
}

int getCatchingSpeed()
{
	/*
	 * Permet de récupérer le type de vitesse de rattrapage appliqué sur l'axe
	 */
	 return _catchingSpeed;
}

void onPadReleased()
{
	/*
	* En ALPHA, au relachement des touches directionnelles on passe en mode vitesse de suivi sauf lors d'un retour de vis (getAxisState() != 11)
	* En DELTA, On arrête le moteur
	*
	* En grande vitesse (GV) on fait un stop() du moteur (voir accelstepper) ce qui correspond à l'état 12 (cf changeAxisState())
	*
	* Mode de Fréquence en mode micro pas (0 = sidéral; 1 = rattrapage PV alpha +, delta +/- ; 2 : rattrapage PV alpha -)
	*/
	if ( getCatchingSpeed() == GV )
	{
		changeAxisState(12);
	}
	else
	{
		if (getAxisType() == ALPHA && getAxisState() != 11)													// cas d'un axe ALPHA : Lancement vitesse suivi
		{
			changeAxisState(3);																												// Vitesse de suivi ALPHA (cf changeAxisState())
		}
		if (getAxisType() == DELTA) 																								// cas d'un axe DELTA : Arret moteur
		{
			changeAxisState(0);																												// Arrêt du moteur (cf changeAxisState())
		}
	}
}

void onWestPress()
{
	/*
	 * Appui sur la direction Ouest
	 *
	 * Mode de vitesse rattrappage (PV = 0 ; MV = 1 ; GV = 2 ; STOP = 3)
	 */
	 if (getAxisState() != 11 )
	 {
		 /*
		  * On ne change le mode du moteur ALPHA que si le mode "retour de vis" n'est pas en cours
		 	*/
			switch (getCatchingSpeed())
			{
				case PV : if (getAxisState() != 6) changeAxisState(5); break; 					// PV rattrapage alpha + (ouest) (cf changeAxisState())
				case MV : changeAxisState(2); break; 																		// MV rattrapage alpha + (ouest) (cf changeAxisState())
				case GV : changeAxisState(4); break; 																		// GV déplacement alpha + (ouest) (cf changeAxisState())
			}
		}
}

void onEastPress()
{
	/*
	 * Appui sur la direction Est
	 *
	 * Mode de vitesse rattrappage (PV = 0 ; MV = 1 ; GV = 2 ; STOP = 3)
	 */
	 if (getAxisState() != 11 )
	 {
		 /*
		  * On ne change le mode du moteur ALPHA que si le mode "retour de vis" n'est pas en cours
		 	*/
			switch (getCatchingSpeed())
			{
				case PV : if (getAxisState() != 5) changeAxisState(6); break;						// PV rattrapage alpha - (est) (cf changeAxisState())
				case MV : changeAxisState(0); break;																		// MV rattrapage alpha - (est) (cf changeAxisState())
				case GV : changeAxisState(7); break;																		// GV déplacement alpha - (est) (cf changeAxisState())
		 }
	}
}

void onSouthPress()
{
	/*
	 * Appui sur la direction Sud
	 *
	 * Mode de vitesse rattrappage (PV = 0 ; MV = 1 ; GV = 2 ; STOP = 3)
	 */
	 switch (getCatchingSpeed())
	 {
		 case PV : changeAxisState(10); break;																			// PV rattrapage Delta + (Sud) (cf changeAxisState())
		 case MV : changeAxisState(2); break;																				// MV rattrapage Delta + (Sud) (cf changeAxisState())
		 case GV : changeAxisState(4); break;																				// GV déplacement Delta + (Sud) (cf changeAxisState())
	 }
}

void onNorthPress()
{
	/*
	 * Appui sur la direction Nord
	 *
	 * Mode de vitesse rattrappage (PV = 0 ; MV = 1 ; GV = 2 ; STOP = 3)
	 */
	 switch (getCatchingSpeed())
	 {
		 case PV : changeAxisState(9); break;																				// PV rattrapage Delta - (Nord) (cf changeAxisState())
		 case MV : changeAxisState(8); break;																				// MV rattrapage Delta - (Nord) (cf changeAxisState())
		 case GV : changeAxisState(7); break;																				// GV déplacement Delta - (Nord) (cf changeAxisState())
	 }
}

void setup()
{
	/*
	 * Configuration des broches
	 */
	 pinMode(SCREW, INPUT);																												// configure la broche SCREW en Entrée (broche utilisée pour la fonction "Retour vis")
	 pinMode(HIGHIMPEDANCE, OUTPUT);																							// configure la broche HIGHIMPEDANCE en Sortie (broche utilisée pour la mise en haute impédance du moteur)
	 if (getAxisType() == ALPHA)
	 {
		 pinMode(TRACKINGMODEINPUT, INPUT);																					// configure la broche TRACKINGMODEINPUT en entrée (broche utilisée pour choisir le mode de vitesse de suivi mais uniquement sur l'axe ALPHA. Sur le DELTA la broche reste disponible pour un autre usage)
	 }
	/*
	 * Configuration liaison série
	 */
	 Serial.begin(9600);
	/*
	 * Envoi message de bienvenue via la liaison série
	 */
	 if ( _debug )
	 {
		 Serial.print("Dinastro MonoMot ");
		 Serial.print(VERSION);
		 if (getAxisType() == ALPHA)																								// cas d'un axe ALPHA
		 {
			 Serial.println(" : Axe ALPHA");
		 }
		 if (getAxisType() == DELTA)																								// cas d'un axe DELTA
		 {
			 Serial.println(" : Axe DELTA");
		 }
	 }
	/*
	 * paramètres par défaut du mode en pas entier (AccelStepper)
	 */
	 stepper.setMaxSpeed(GVSPEED);
	 stepper.setAcceleration(ACCELERATIONCOEF);
	 stepper.setSpeed(MVSPEED);
	 stepper.setCurrentPosition(0);																								// TODO : Au lancement lire la position dans la mémoire de stockage (dernière position connue)
	/*
	 * Désactivation du moteur à la mise sous tension
	 */
	 disableStepper();
	/*
	 * Configuration du Timer d'interruption périodique
	 */
	 FrequencyTimer2::setOnOverflow(toogleOutput);																// Association de l'interruption avec la fonction de traitement des micropas et pas entiers (toogleOutput)
	/*
	 * Configuration du Timer des PWM
	 */
	 Timer1.initialize(17);																												// initialize timer1 : 17 microseconds soit 58.8kHz (c'est la fréquence des PWM)
	/*
	 * Affection du mode moteur au lancement
	 */
	 if (getAxisType() == ALPHA)																									// Cas d'un axe ALPHA : Moteur en vitesse de suivi sidérale au lancement
	 {
		 changeAxisState(3);																												// Vitesse de suivi ALPHA (cf changeAxisState())
	 }
	 if (getAxisType() == DELTA)																									// Cas d'un axe DELTA : Moteur à l'arrêt au lancement
	 {
		 changeAxisState(0);																												// Moteur à l'arret (cf changeAxisState())
	 }
}

void loop()
{
	/*
	 * Mise à jour du status de la raquette (lecture de l'état)
	 */
	 pad.updateStatus();
	/*
	 * Gestion de l'entrée "Retour vis" uniquement sur l'axe ALPHA
	 * Le fonctionnement de l'axe ALPHA est inchangé tant que l'entrée SCREW est à 1 logique
	 * Si l'entrée logique SCREW est au zéro logique, on applique une vitesse GV sur l'axe ALPHA en sens retour (opposé au sens de suivi)
	 */
	 if (getAxisType() == ALPHA && digitalRead(SCREW) == HIGH && getAxisState() == 11)
	 {
		 /*
		  * L'entrée SCREW est à l'état 1 logique (+Vcc) : on annule le mode "retour de vis"
			* et
			* On ralenti la vitesse GV (rampe) puis On active le suivi en alpha
			*/
			changeAxisState(12);																											// Activation de la rampe de ralentissement (cf changeAxisState())
			if ( _debug )
			{
				Serial.println("Fin du mode retour de vis");
			}
	 }
	 if (getAxisType() == ALPHA && digitalRead(SCREW) == LOW && getAxisState() != 11)
	 {
		 /*
		  * L'entrée SCREW est à l'état 0 logique (Ground) : on active le mode "retour de vis"
			* et
			* On active le retour en vitesse GV sur l'ALPHA
			*/
			changeAxisState(11);																											// Retour vis sens - (est) (cf changeAxisState())
			if ( _debug )
			{
				Serial.println("Début du mode retour de vis");
			}
	 }
	/*
	 * Gestion de l'entrée mode de suivi sur le connecteur d'extension uniquement pour l'axe ALPHA
	 */
	 if (getAxisType() == ALPHA && digitalRead(TRACKINGMODEINPUT) == HIGH && getTrackingMode() == STELLARTRACKING)
	 {
		 /*
		  * l'entrée TRACKINGMODEINPUT est à l'état logique 1 (+vcc) : on passe en mode de suivi lunaire
			*/
			setTrackingMode(MOONTRACKING);
			/*
			 * On active le suivi en alpha
			 */
			 changeAxisState(3);																											// Vitesse de suivi ALPHA (cf changeAxisState())
			 if ( _debug )
			 {
				 Serial.println("Passage en mode de suivi Lunaire");
			 }
	 }
	 if (getAxisType() == ALPHA && digitalRead(TRACKINGMODEINPUT) == LOW && getTrackingMode() == MOONTRACKING)
	 {
		 /*
		  * l'entrée trackingModeINPUT est à l'état logique 1 (+vcc) : on passe en mode de suivi sidéral
			*/
			setTrackingMode(STELLARTRACKING);
		 /*
		  * On active le suivi en alpha
			*/
			changeAxisState(3);																												// Vitesse de suivi ALPHA (cf changeAxisState())
			if ( _debug )
			{
				Serial.println("Passage en mode de suivi Stellaire");
			}
	 }
	 if ( _debug )
	 {
		/*
		 * Fonction de test de la position
		 * /!\ risque de saturation du processeur et de la console série /!\
		 */
		 //Serial.print("Position courante : ");
		 //Serial.println(getCurrentPosition());
	 }
}
