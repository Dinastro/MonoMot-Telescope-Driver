#include "DinastroAxis.h"

// Constructors/Destructors
//  
/**
 * Constructeur de la classe DinastroAxis
 * @param  aStepper Une référence vers un controleur de moteur pas à pas de type
 * AccelStepper
 * @param  dMStepper Une référence vers un controleur de moteur pas à pas de type
 * DinastroMicroStepper
 * @param  pad Une référence vers un controleur de raquette de type DinastroPad
 */
 DinastroAxis::DinastroAxis (AccelStepper aStepper = null, DinastroMicroStepper dMStepper = null, DinastroPad pad = null )
{
	_dMStepper = aStepper;
	_aStepper = dMStepper;
	_pad = pad;
}

//  
// Methods
//  


// Accessor methods
//  


// Public static attribute accessor methods
//  


// Public attribute accessor methods
//  


// Protected static attribute accessor methods
//  


// Protected attribute accessor methods
//  


// Private static attribute accessor methods
//  


// Private attribute accessor methods
//  

// Other methods
//  


/**
 * Méthode exécutée périodiquement par un timer. Permettant de rafraichir l'état
 * des sorties du microC destinées à commander les moteurs.
 */
void DinastroAxis::compute ( )
{
}


/**
 * Méthode lancée à intervalles réguliers permettant de prendre en compte les
 * changements d'états de la raquette
 */
void DinastroAxis::updateAxisStatus ( )
{
}

/**
 * @param  catchUPSpeed Vitesse de rattrapage :
 * 0 : Pv (défaut)
 * 1 : Mv
 * 2 : Gv
 */
void DinastroAxis::setCatchUpSpeed (int catchUPSpeed = 0 )
{
}


/**
 * @param  direction Sens du rattrapage :
 * 0 : sens positif (alpha+ ou delta+)
 * 1 : sens négatif (alpha- ou delta-)
 */
void DinastroAxis::launchCatchingUp (int direction )
{
}

