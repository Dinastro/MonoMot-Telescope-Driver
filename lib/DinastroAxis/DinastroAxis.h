
#ifndef DinastroAxis_h
#define DinastroAxis_h

#if ARDUINO >= 100
 #include "Arduino.h"
#else
 #include "WProgram.h"
 #include "stdlib.h"
#endif

#include <DinastroPad.h>
#include <AccelStepper.h>

/**
  * class DinastroAxis
  * Classe qui permet de gérer un axe (alpha ou delta). Il ne peut y avoir qu'un
  * seul axe par carte DinastroMot.
  */

class DinastroAxis
{
public:

		// Constructors/Destructors
		//  
		/**
		 * Constructeur de la classe DinastroAxis
		 * @param  aStepper Une référence vers un controleur de moteur pas à pas de type
		 * AccelStepper
		 * @param  dMStepper Une référence vers un controleur de moteur pas à pas de type
		 * DinastroMicroStepper
		 * @param  pad Une référence vers un controleur de raquette de type DinastroPad
		 */
		 DinastroAxis (AccelStepper aStepper = null, DinastroMicroStepper dMStepper = null, DinastroPad pad = null );


		/**
		 * Méthode exécutée périodiquement par un timer. Permettant de rafraichir l'état
		 * des sorties du microC destinées à commander les moteurs.
		 */
		void compute ( );


		/**
		 * Méthode lancée à intervalles réguliers permettant de prendre en compte les
		 * changements d'états de la raquette
		 */
		void updateAxisStatus ( );

protected:

		// Static Protected attributes
		//  

		// Protected attributes
		//  

public:


		// Protected attribute accessor methods
		//  

protected:

public:


		// Protected attribute accessor methods
		//  

protected:


private:

		// Static Private attributes
		//  

		// Private attributes
		//  

		DinastroMicroStepper _dMStepper;
		AccelStepper _aStepper;
		DinastroPad _pad;
public:


		// Private attribute accessor methods
		//  

private:

public:


private:



		/**
		 * @param  catchUPSpeed Vitesse de rattrapage :
		 * 0 : Pv (défaut)
		 * 1 : Mv
		 * 2 : Gv
		 */
		void setCatchUpSpeed (int catchUPSpeed = 0 );


		/**
		 * @param  direction Sens du rattrapage :
		 * 0 : sens positif (alpha+ ou delta+)
		 * 1 : sens négatif (alpha- ou delta-)
		 */
		void launchCatchingUp (int direction );

		void initAttributes ( ) ;

};

#endif
