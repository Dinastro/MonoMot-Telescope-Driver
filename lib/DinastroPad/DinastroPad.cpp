// DinastroPad.cpp
// Version 2
// JPG 22/1/2014 : Détection des appuis et relachement des touches

//#include "WProgram.h"
#include "DinastroPad.h"

DinastroPad::DinastroPad(void (*onLowSpeedButtonPress)(),
							void (*onLowSpeedButtonRelease)(),
							void (*onMediumSpeedButtonPress)(),
							void (*onMediumSpeedButtonRelease)(),
							void (*onHighSpeedButtonPress)(),
							void (*onHighSpeedButtonRelease)(),
							void (*onWestButtonPress)(),
							void (*onWestButtonRelease)(),
							void (*onEastButtonPress)(),
							void (*onEastButtonRelease)(),
							void (*onSouthButtonPress)(),
							void (*onSouthButtonRelease)(),
							void (*onNorthButtonPress)(),
							void (*onNorthButtonRelease)()
						)
{
	_bWest = false;
	_bEast = false;
	_bSouth = false;
	_bNorth = false;
	_bLowSpeed = false;
	_bMediumSpeed = false;
	_bHighSpeed = false;
	_keyStatus = false;
	_padPreviousStates = 0;  // mémorise l'état précédent des boutons de la raquette : bit0 : LowSpeed ; bit1 : MediumSpeed ; Bit2 : HighSpeed ; Bit3 : West ; Bit 4 : Est ; Bit 5 ; South ; Bit 6 : North ; Bit 7 : libre
	_onLowSpeedButtonPress = onLowSpeedButtonPress;
	_onLowSpeedButtonRelease = onLowSpeedButtonRelease;
	_onMediumSpeedButtonPress = onMediumSpeedButtonPress;
	_onMediumSpeedButtonRelease = onMediumSpeedButtonRelease;
	_onHighSpeedButtonPress = onHighSpeedButtonPress;
	_onHighSpeedButtonRelease = onHighSpeedButtonRelease;
	_onWestButtonPress = onWestButtonPress;
	_onWestButtonRelease = onWestButtonRelease;
	_onEastButtonPress = onEastButtonPress;
	_onEastButtonRelease = onEastButtonRelease;
	_onSouthButtonPress = onSouthButtonPress;
	_onSouthButtonRelease = onSouthButtonRelease;
	_onNorthButtonPress = onNorthButtonPress;
	_onNorthButtonRelease = onNorthButtonRelease;
}

boolean DinastroPad::getWestButton()
{
	return _bWest;
}

boolean DinastroPad::getEastButton()
{
	return _bEast;
}

boolean DinastroPad::getSouthButton()
{
	return _bSouth;
}

boolean DinastroPad::getNorthButton()
{
	return _bNorth;
}

boolean DinastroPad::getLowSpeedButton()
{
	return _bLowSpeed;
}

boolean DinastroPad::getMediumSpeedButton()
{
	return _bMediumSpeed;
}

boolean DinastroPad::getHighSpeedButton()
{
	return _bHighSpeed;
}

boolean DinastroPad::getkeyStatus()
{
	return _keyStatus;
}

void DinastroPad::updateStatus()
{
	/* Scrutation des entrées de la raquette */
	_keyStatus = false;                   // par défaut status relaché (passe à un uniquement si une bouton est appuyé)
	_bWest = false;
	_bEast = false;
	_bSouth = false;
	_bNorth = false;
	_bLowSpeed = false;
	_bMediumSpeed = false;
	_bHighSpeed = false;

	if (!digitalRead(2) && digitalRead(3)) // Ouest
	{
		_bWest = true;
		_keyStatus = true;
		if ( !bitRead(_padPreviousStates,3) )
		{
			// Lancement événement "sur appui Touche West"
			_onWestButtonPress();
		}
		bitSet(_padPreviousStates,3);
	}

	if (digitalRead(2) && !digitalRead(3)) // Est
	{
		_bEast = true;
		_keyStatus = true;
		if ( !bitRead(_padPreviousStates,4) )
		{
			// Lancement événement "sur appui Touche East"
			_onEastButtonPress();
		}
		bitSet(_padPreviousStates,4);
	}

	if (!digitalRead(4) && digitalRead(5)) // Sud
	{
		_bSouth = true;
		_keyStatus = true;
		if ( !bitRead(_padPreviousStates,5) )
		{
			// Lancement événement "sur appui Touche South"
			_onSouthButtonPress();
		}
		bitSet(_padPreviousStates,5);
	}

	if (digitalRead(4) && !digitalRead(5)) // Nord
	{
		_bNorth = true;
		_keyStatus = true;
		if ( !bitRead(_padPreviousStates,6) )
		{
			// Lancement événement "sur appui Touche North"
			_onNorthButtonPress();
		}
		bitSet(_padPreviousStates,6);
	}

	if (!digitalRead(2) && !digitalRead(3)) // PV
	{
		_bLowSpeed = true;
		_keyStatus = true;
		if ( !bitRead(_padPreviousStates,0) )
		{
			// Lancement événement "sur appui Touche LowSpeed"
			_onLowSpeedButtonPress();
		}
		bitSet(_padPreviousStates,0);
	}

	if (!digitalRead(4) && !digitalRead(5)) // MV
	{
		_bMediumSpeed = true;
		_keyStatus = true;
		if ( !bitRead(_padPreviousStates,1) )
		{
			// Lancement événement "sur appui Touche MediumSpeed"
			_onMediumSpeedButtonPress();
		}
		bitSet(_padPreviousStates,1);
	}

	if (!digitalRead(6)) // GV
	{
		_bHighSpeed = true;
		_keyStatus = true;
		if ( !bitRead(_padPreviousStates,2) )
		{
			// Lancement événement "sur appui Touche HighSpeed"
			_onHighSpeedButtonPress();
		}
		bitSet(_padPreviousStates,2);
	}
	
    if ( !_keyStatus )
    {
		/*
		/ Détection d'un évenement (relachement des touches)
		*/
		if ( bitRead(_padPreviousStates,3) && !_bWest )
		{
			// Détection de relachement du bouton West
			bitClear(_padPreviousStates, 3);
			_onWestButtonRelease();
		}
		if ( bitRead(_padPreviousStates,4) && !_bEast )
		{
			// Détection de relachement du bouton East
			bitClear(_padPreviousStates, 4);
			_onEastButtonRelease();
		}
		if ( bitRead(_padPreviousStates,5) && !_bSouth )
		{
			// Détection de relachement du bouton South
			bitClear(_padPreviousStates, 5);
			_onSouthButtonRelease();
		}
		if ( bitRead(_padPreviousStates,6) && !_bNorth )
		{
			// Détection de relachement du bouton North
			bitClear(_padPreviousStates, 6);
			_onNorthButtonRelease();
		}
		if ( bitRead(_padPreviousStates,0) && !_bLowSpeed )
		{
			// Détection de relachement du bouton LowSpeed
			bitClear(_padPreviousStates, 0);
			_onLowSpeedButtonRelease();
		}
		if ( bitRead(_padPreviousStates,1) && !_bMediumSpeed )
		{
			// Détection de relachement du bouton MediumSpeed
			bitClear(_padPreviousStates, 1);
			_onMediumSpeedButtonRelease();
		}
		if ( bitRead(_padPreviousStates,2) && !_bHighSpeed )
		{
			// Détection de relachement du bouton HighSpeed
			bitClear(_padPreviousStates, 2);
			_onHighSpeedButtonRelease();
		}
	}
}
