// DinastroPad.h version 2
// JPG 22/1/2014 : Détection des appuis et relachement des touches
 
#ifndef DinastroPad_h
#define DinastroPad_h

#if ARDUINO >= 100
 #include "Arduino.h"
#else
 #include "WProgram.h"
 #include "stdlib.h"
#endif

class DinastroPad
{
public:
	DinastroPad(void (*onLowSpeedButtonPress)(),
				void (*onLowSpeedButtonRelease)(),
				void (*onMediumSpeedButtonPress)(),
				void (*onMediumSpeedButtonRelease)(),
				void (*onHighSpeedButtonPress)(),
				void (*onHighSpeedButtonRelease)(),
				void (*onWestButtonPress)(),
				void (*onWestButtonRelease)(),
				void (*onEastButtonPress)(),
				void (*onEastButtonRelease)(),
				void (*onSouthButtonPress)(),
				void (*onSouthButtonRelease)(),
				void (*onNorthButtonPress)(),
				void (*onNorthButtonRelease)()
				);
	boolean getWestButton();
	boolean getEastButton();
	boolean getSouthButton();
	boolean getNorthButton();
	boolean getLowSpeedButton();
	boolean getMediumSpeedButton();
	boolean getHighSpeedButton();
	boolean getkeyStatus();
	void updateStatus();

private:
	boolean _bWest;         	// status du bouton Ouest
	boolean _bEast;         	// status du bouton Est
	boolean _bSouth;        	// status du bouton Sud
	boolean _bNorth;        	// status du bouton Nord
	boolean _bLowSpeed;     	// status du bouton Petite Vitesse
	boolean _bMediumSpeed;  	// status du bouton Moyenne Vitesse
	boolean _bHighSpeed;    	// status du bouton Grande Vitesse
	boolean _keyStatus;     	// status des touches de la raquette (true -> appuyées ; false -> relachées)
	byte _padPreviousStates;	// mémorise l'état précédent des boutons de la raquette : bit0 : LowSpeed ; bit1 : MediumSpeed ; Bit2 : HighSpeed ; Bit3 : West ; Bit 4 : Est ; Bit 5 ; South ; Bit 6 : North ; Bit 7 : libre
    void (*_onLowSpeedButtonPress)();			// Une référence vers la procédure lancée lors de l'événement onLowSpeedButtonPress
    void (*_onLowSpeedButtonRelease)();			// Une référence vers la procédure lancée lors de l'événement onLowSpeedButtonRelease
    void (*_onMediumSpeedButtonPress)();		// Une référence vers la procédure lancée lors de l'événement onMediumSpeedButtonPress
    void (*_onMediumSpeedButtonRelease)();		// Une référence vers la procédure lancée lors de l'événement onMediumSpeedButtonRelease
    void (*_onHighSpeedButtonPress)();			// Une référence vers la procédure lancée lors de l'événement onHighSpeedButtonPress
    void (*_onHighSpeedButtonRelease)();		// Une référence vers la procédure lancée lors de l'événement onHighSpeedButtonRelease
    void (*_onWestButtonPress)();				// Une référence vers la procédure lancée lors de l'événement onWestButtonPress
    void (*_onWestButtonRelease)();				// Une référence vers la procédure lancée lors de l'événement onWestButtonRelease
    void (*_onEastButtonPress)();				// Une référence vers la procédure lancée lors de l'événement onEastButtonPress
    void (*_onEastButtonRelease)();				// Une référence vers la procédure lancée lors de l'événement onEastButtonRelease
    void (*_onSouthButtonPress)();				// Une référence vers la procédure lancée lors de l'événement onSouthButtonPress
    void (*_onSouthButtonRelease)();			// Une référence vers la procédure lancée lors de l'événement onSouthButtonRelease
    void (*_onNorthButtonPress)();				// Une référence vers la procédure lancée lors de l'événement onNorthButtonPress
    void (*_onNorthButtonRelease)();			// Une référence vers la procédure lancée lors de l'événement onNorthButtonRelease
};
#endif 
