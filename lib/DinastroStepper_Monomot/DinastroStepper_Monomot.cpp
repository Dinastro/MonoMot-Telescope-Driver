#include "DinastroStepper_Monomot.h"
#include <util/atomic.h>     								// Permet d'utiliser les accès atomiques aux varibles globales

// Constructors/Destructors
//  

DinastroStepper::DinastroStepper (int stepper_step,
									unsigned long timer_period,
									unsigned long tracking_period,
									unsigned long est_centering_period,
									unsigned long west_centering_period,
									unsigned long pointing_period,
									unsigned long catch_up_period,
									unsigned long accel_period,
									unsigned long accel_step_pss,
									unsigned long accel_step_overcome) {
	_stepper_step = stepper_step;
	_timer_period = timer_period;
	_tracking_period = tracking_period;
	_est_centering_period = est_centering_period;
	_west_centering_period = west_centering_period;
	_pointing_period = pointing_period;
	_catch_up_period = catch_up_period;
	_accel_period = accel_period;
	_accel_step_pss = accel_step_pss;
	_accel_step_overcome = accel_step_overcome;
	
	initStepper();

}

DinastroStepper::~DinastroStepper () { }

//  
// Methods
//  


// Accessor methods
//  


// Other methods
//  


/**
 * Initialisation du moteur pas à pas.
 * 
 * Initialise les connexions vers l'étage de sortie ;
 * Initialise les variables globales gérants les paramètres moteurs ;
 * Place le moteur pas à pas en conditions initiale ;
 * Active le moteur pas à pas.
 */
void DinastroStepper::initStepper ()
{
	/* Initialisation des connexions vers l'étage de sortie */
	/*pinMode(DinastroStepper::DIR, OUTPUT);     
	pinMode(DinastroStepper::STEP, OUTPUT);
	pinMode(DinastroStepper::MS1, OUTPUT);     
	pinMode(DinastroStepper::MS2, OUTPUT);
	pinMode(DinastroStepper::ENABLE, OUTPUT);*/
	pinMode(DinastroStepper::CCP1, OUTPUT);
	pinMode(DinastroStepper::CCP2, OUTPUT);
	pinMode(DinastroStepper::CNT1, OUTPUT);
	pinMode(DinastroStepper::CNT2, OUTPUT);

	disableStepper();
                                             
	/* Initialisation des variables globales moteur */
	_CurrentSteppingPeriodicCounter = 0;
	_StepperReferencePeriod[0] = long((float)_tracking_period / (float)_timer_period);
	_StepperReferencePeriod[1] = long((float)_est_centering_period / (float)_timer_period);
	_StepperReferencePeriod[2] = long((float)_west_centering_period / (float)_timer_period);
	_StepperReferencePeriod[3] = long((float)_catch_up_period / (float)_timer_period);
	_StepperReferencePeriod[4] = long((float)_pointing_period / (float)_timer_period);
	_AccelReferencePeriod = long((float)_accel_period / (float)_timer_period);
	_AccelCounterOrder = _AccelReferencePeriod;
	_MaxAccelSpeedPeriodicCounter = convertCounterValueFromFrequency(_StepperReferencePeriod[3]);
	_MinAccelSpeedPeriodicCounter = convertCounterValueFromFrequency(_StepperReferencePeriod[0] / 1);

	if ( _accel_step_pss > 100)
	{
		_AccelTimerCounterCoef = (long)((float)_accel_step_pss / ( (float)_accel_period / (float)_timer_period));
	}
	else
	{	
		_AccelTimerCounterCoef = 100;		
	}
	if (_AccelTimerCounterCoef <= 2)
	{
		_AccelTimerCounterCoef = 2;	
	}
	if ( _AccelTimerCounterCoef > 50 )
	{
		// _AccelTimerCounterCoefB est 50 fois plus petit que _AccelTimerCounterCoef
		_AccelTimerCounterCoefB = long((float)_AccelTimerCounterCoef / 50);		
	}
	else
	{	
		_AccelTimerCounterCoefB = 2;		
	}

	/* Reset de la position */	
	resetPosition();

	/* Activation du moteur */
	enableStepper();
}


/**
 * Activation du moteur pas à pas.
 * 
 * Basculement de la sortie ENABLE à l'état bas du circuit de commande moteur ;
 * Activation du Timer2 (échantillonnage des pas et des rampes).
 */
void DinastroStepper::enableStepper ()
{
	digitalWrite(DinastroStepper::ENABLE, LOW); 
	_StepperEnabled = LOW;
}


/**
 * Désactivation du moteur pas à pas.
 * 
 * Basculement de la sortie ENABLE à l'état haut du circuit de commande moteur ;
 * Désactivation du Timer2 (échantillonnage des pas et des rampes).
 */
void DinastroStepper::disableStepper ()
{
	digitalWrite(DinastroStepper::ENABLE, HIGH);
	_StepperEnabled = HIGH;
}


/**
 * Retourne la position en nombre de pas, par rapport à l'initilisation du moteur
 * (type int).
 * @return int
 */
int DinastroStepper::getPosition ()
{
	return _Pos;
}


/**
 * Initialise à zéro la position du moteur.
 */
void DinastroStepper::resetPosition ()
{
	_Pos = 0;
}


/**
 * Retourne le mode de commande du moteur pas à pas (type int).
 * 
 * Le moteur pas à pas peut fonctionner en mode pas entier, demi ou 8eme de pas.
 * La méthode retourne un int correspondant au mode selon les constantes logiques
 * suivantes :
 * FULL = 1			Mode moteur pas entier
 * HALF = 2			Mode moteur en demi pas
 * HEIGHT = 8			Mode moteur en 8éme de pas
 * @return int
 */
int DinastroStepper::getStepperMode ()
{
	return _StepperMode;
}


/**
 * Retourne le sens de rotation du moteur (type int).
 * 
 * La méthode retourne un int correspondant au sens selon les constantes logiques
 * suivantes :
 * CLOCKWISE = 0		Sens de rotation des aiguilles d'une montre (mode par défaut du
 * suivi)
 * C_CLOCKWISE = 1		Sens de rotation inverse des aiguilles d'une montre
 * @return int
 */
int DinastroStepper::getDirection ()
{
	return _Dir;
}


/**
 * Retourne l'état logique du moteur (type int).
 * 
 * La méthode retourne un int correspondant à l'état logique de fonctionnement du
 * moteur selon les constantes logiques suivantes :
 * TRACKING_STATE = 0		en vitesse de suivi
 * WEST_CENTERING_STATE = 1		en vitesse de centrage vers l'Ouest
 * EST_CENTERING_STATE = 2		en vitesse de centrage vers l'Est
 * WEST_POINTING_STATE = 3		en vitesse de pointage vers l'Ouest
 * EST_POINTING_STATE = 4		en vitesse de pointage vers l'Est
 * WEST_CATCH_UP_STATE = 5		en vitesse de rattrapage vers l'Ouest
 * EST_CATCH_UP_STATE = 6		en vitesse de rattrapage vers l'Est
 * RETURNING_TO_TRACKING_STATE = 7	dans l'état de retour vers la vitesse de suivi
 * (ralentissement depuis une vitesse plus rapide)
 * @return int
 */
int DinastroStepper::getState ()
{
	return _State;
}


/**
 * Lancement de la vitesse de suivi.
 */
void DinastroStepper::launchTracking ()
{
	if ( _State != DinastroStepper::TRACKING_STATE )
	{
		setStepperMode(DinastroStepper::HEIGHT);
		changeDirection(DinastroStepper::CLOCKWISE);  
		_SteppingPeriodicCounterOrder = convertCounterValueFromFrequency(_StepperReferencePeriod[0]);
		_AccelSens = DinastroStepper::CONSTANT;
		_State = DinastroStepper::TRACKING_STATE;
		enableStepper();	
	}
}


/**
 * Lancement de la vitesse de centrage vers L'Ouest.
 */
void DinastroStepper::launchWestCentering ()
{
	if ( _State != DinastroStepper::WEST_CENTERING_STATE )
	{
		setStepperMode(DinastroStepper::HEIGHT);
		changeDirection(DinastroStepper::CLOCKWISE);  
		_SteppingPeriodicCounterOrder = convertCounterValueFromFrequency(_StepperReferencePeriod[1]);
		_AccelSens = DinastroStepper::CONSTANT;
		_State = DinastroStepper::WEST_CENTERING_STATE;
		enableStepper();
	}
}


/**
 * Lancement de la vitesse de centrage vers l'Est.
 */
void DinastroStepper::launchEstCentering ()
{
	if ( _State != DinastroStepper::EST_CENTERING_STATE ) 
	{
		setStepperMode(DinastroStepper::HEIGHT);
		changeDirection(DinastroStepper::CLOCKWISE);  
		_SteppingPeriodicCounterOrder = convertCounterValueFromFrequency(_StepperReferencePeriod[2]);
		_AccelSens = DinastroStepper::CONSTANT;
		_State = DinastroStepper::EST_CENTERING_STATE;
		enableStepper();
	}
}


/**
 * Lancement de la vitesse de pointage vers l'Est.
 */
void DinastroStepper::launchEstPointing ()
{
	if ( _State != DinastroStepper::EST_POINTING_STATE ) 
	{
		setStepperMode(DinastroStepper::HEIGHT);
		changeDirection(DinastroStepper::C_CLOCKWISE);  
		_SteppingPeriodicCounterOrder = convertCounterValueFromFrequency(_StepperReferencePeriod[4]);
		_AccelSens = DinastroStepper::CONSTANT;
		_State = DinastroStepper::EST_POINTING_STATE;
		enableStepper();
	}
}


/**
 * Lancement de la vitesse de pointage vers l'Ouest.
 */
void DinastroStepper::launchWestPointing ()
{
	if ( _State != DinastroStepper::WEST_POINTING_STATE ) 
	{
		setStepperMode(DinastroStepper::HEIGHT);
		changeDirection(DinastroStepper::CLOCKWISE);
		_SteppingPeriodicCounterOrder = convertCounterValueFromFrequency(_StepperReferencePeriod[4]); 
		_State = DinastroStepper::WEST_POINTING_STATE; 
		_AccelSens = DinastroStepper::CONSTANT;
		enableStepper();
	}
}


/**
 * Lancement de la vitesse de rattrapage vers l'Ouest.
 */
void DinastroStepper::launchWestCatchUP ()
{
	if ( _State != DinastroStepper::WEST_CATCH_UP_STATE )
	{
		setStepperMode(DinastroStepper::FULL);
		changeDirection(DinastroStepper::CLOCKWISE);  
		_AccelSens = DinastroStepper::UP;
		_SteppingPeriodicCounterOrder = convertCounterValueFromFrequency(_StepperReferencePeriod[0]);
		_State = DinastroStepper::WEST_CATCH_UP_STATE;
		enableStepper();
	}
}


/**
 * Lancement de la vitesse de rattrapage vers l'Est.
 */
void DinastroStepper::launchEstCatchUP ()
{
	if ( _State != DinastroStepper::EST_CATCH_UP_STATE ) 
	{
		setStepperMode(DinastroStepper::FULL);
		changeDirection(DinastroStepper::C_CLOCKWISE);  
		_AccelSens = DinastroStepper::UP;
		_SteppingPeriodicCounterOrder = convertCounterValueFromFrequency(_StepperReferencePeriod[0]);
		_State = DinastroStepper::EST_CATCH_UP_STATE;
		enableStepper();
	}
}


/**
 * Lancement de la procédure de retour à la vitesse de suivi après une rotation
 * rapide.
 * 
 * Lance une rampe de ralentissement.
 */
void DinastroStepper::launchReturnToTracking ()
{
	if ( _State != DinastroStepper::RETURNING_TO_TRACKING_STATE ) 
	{
		setStepperMode(DinastroStepper::FULL);
		changeDirection(_Dir);  
		_AccelSens = DinastroStepper::DOWN;
		_State = DinastroStepper::RETURNING_TO_TRACKING_STATE;
		enableStepper();
	}
}


/**
 * Mise à jour de la position angulaire (en pas).
 * 
 * Met à jour la position en nombre de pas par rapport au point de départ (voir
 * initStepper()).
 */
void DinastroStepper::updatePosition ()
{
	if (_StepperEnabled == LOW)
	{
		switch (_Dir)
		{
			case DinastroStepper::CLOCKWISE:
				_Pos++;
				break;
			case DinastroStepper::C_CLOCKWISE:
				_Pos--;
				break;
		}    
		if ((_Pos == (_stepper_step * _StepperMode) && (_Dir == DinastroStepper::CLOCKWISE)))
		{	
			_Pos = 0;
		}
		if ((_Pos == -1) && (_Dir == DinastroStepper::C_CLOCKWISE))
		{
			_Pos = (_stepper_step * _StepperMode - 1);
		}   
	}
}


/**
 * Modification du mode de commande du moteur pas à pas.
 * 
 * Le moteur pas à pas peut fonctionner en mode pas entier, demi ou 8eme de pas.
 * La méthode prend un int en paramètre correspondant au mode selon les constantes
 * logiques suivantes :
 * FULL = 1			Mode moteur pas entier
 * HALF = 2			Mode moteur en demi pas
 * HEIGHT = 8			Mode moteur en 8éme de pas
 * @param  mode
 */
void DinastroStepper::setStepperMode (int mode)
{
	int oldMode = _StepperMode;
	_StepperMode = mode;
	switch(mode)
	{
		case DinastroStepper::FULL:
			digitalWrite(DinastroStepper::MS1, LOW);
			digitalWrite(DinastroStepper::MS2, LOW);
			break;
			
		case DinastroStepper::HALF:
			digitalWrite(DinastroStepper::MS1, HIGH);
			digitalWrite(DinastroStepper::MS2, LOW);
			break;
			
		case DinastroStepper::HEIGHT:
			digitalWrite(DinastroStepper::MS1, HIGH);
			digitalWrite(DinastroStepper::MS2, HIGH);
		break;
	}
	_Pos = _Pos * _StepperMode / oldMode;
}


/**
 * Change le sens de rotation du moteur.
 * 
 * Modifie la valeur de la sortie DIR de l'étage de sortie.
 * La méthode prend en paramètre un int correspondant au sens selon les constantes
 * logiques suivantes :
 * CLOCKWISE = 0		Sens de rotation des aiguilles d'une montre (mode par défaut du
 * suivi)
 * C_CLOCKWISE = 1		Sens de rotation inverse des aiguilles d'une montre
 * @param  dir
 */
void DinastroStepper::changeDirection (int dir)
{
	_Dir = dir;
	digitalWrite(DinastroStepper::DIR, dir);
}


/**
 * Convertie une période en micro-secondes en nombre de passages dans la procédure
 * d'échantillonage périodique.
 * 
 * Prend un long en paramètre représentant la période en micro secondes.
 * Retourne un long représentant le nombre de passages dans la procédure
 * d'échantillonage périodique.
 * @return long
 * @param  aFrequency
 */
long DinastroStepper::convertCounterValueFromFrequency (long aFrequency)
{
	return aFrequency * 2;
}


/**
 * Réalise les calculs d'actialisation de la vitesse de rotation.
 * 
 * Assure l'augmentation ou la diminution de la vitesse de rotation selon la
 * position dans le cycle d'accélération ou de ralentissement.
 */
void DinastroStepper::compute ()
{
	unsigned long lTimerCounter;
	unsigned long lDelta;

	ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
	{
		/*
		 * Section où les variables globales sont accédées en mode atomic (désactivation des interruptions)
		 * Les interruptions sont désactivées de façon à ne pas créer d'incohérences 
		 */
		if ( (_AccelSens == DinastroStepper::UP) ||  (_AccelSens == DinastroStepper::DOWN) )
		{
			_AccelOvercomeCounter++;
			lTimerCounter = _SteppingPeriodicCounterOrder;
			if (_AccelOvercomeCounter >= _accel_step_overcome)
			{
				switch (_AccelSens)
				{
					case UP:
						lDelta = _MaxAccelSpeedPeriodicCounter - lTimerCounter;
						if ( lDelta > _AccelTimerCounterCoef )
						{
							lDelta = _AccelTimerCounterCoefB;
						}
						lTimerCounter = lTimerCounter - lDelta;
						if ( (lTimerCounter <= _MaxAccelSpeedPeriodicCounter) || ( lTimerCounter <=0 ))
						{
							lTimerCounter = _MaxAccelSpeedPeriodicCounter;
						}
						break;
						
					case DOWN:
						lDelta = lTimerCounter - _MinAccelSpeedPeriodicCounter;
						if ( lDelta > _AccelTimerCounterCoef )
						{	
							lDelta = _AccelTimerCounterCoefB;
						}
						lTimerCounter = lTimerCounter + lDelta;
						if ( lTimerCounter >= _MinAccelSpeedPeriodicCounter )
						{
							lTimerCounter = _MinAccelSpeedPeriodicCounter;
							launchTracking();    // Retour en mode suivi
						}
						break;
				}
				_AccelOvercomeCounter = 0;
			}
			_SteppingPeriodicCounterOrder = lTimerCounter;
		}
	} 
}


/**
 * Procédure lancée par le Timer2 (ISR : Interrupt Service Routine).
 * Basculement des sorties de commande de la broche STEP de l'étage de puissance et
 * lancement des procédures périodiques.
 * 
 * Boucle synchrone en mode interruption sur Timer2.
 * Toutes les TIMER_PERIOD micro-secondes on parcours cette fonction.
 * Selon la vitesse de rotation voulue on compare le
 * _CurrentSteppingPeriodicCounter à une valeur de frequence constante.
 * On procède de mme pour le calcul de la vitesse en accélération ou
 * ralentissement.
 */
void DinastroStepper::toogleOutput ()
{
	if (_StepperEnabled == LOW)
	{
		/*
		 * Mise à jour des compteurs synchones
		 */
		_CurrentSteppingPeriodicCounter++;
		_CurrentAccelCounter++;

		/* 
		 * Traitement de la sortie de l'étage de puissance
		 */
		if (_CurrentSteppingPeriodicCounter >= _SteppingPeriodicCounterOrder)
		{
			_CurrentSteppingPeriodicCounter = 0;
			monomotDriver();      
		}
		/* 
		 * Traitement du calcul de la vitesse de rotation en phase d'accelération ou ralentissement
		 */
		if (_CurrentAccelCounter >= _AccelCounterOrder) 
		{
			_CurrentAccelCounter = 0;
			compute();
		}
	}
}


/**
 * Procédure lancée par la méthode toogleOutput.
 * Elle n'est utilisée que pour commander un driver 
 * de moteur pas-à-pas selon la platine Monomot
 * (version > 2.52Pro)
 */
void DinastroStepper::monomotDriver ()
{
	_StepLevel++;
	if (_StepLevel == 32)
	{
		_StepLevel = 0;
	}
	analogWrite(CCP1,_CCP1_fullStep_Values[_StepLevel]);
	analogWrite(CCP2,_CCP2_fullStep_Values[_StepLevel]);
	digitalWrite(CNT1,_CNT1_Values[_StepLevel]);
	digitalWrite(CNT2,_CNT2_Values[_StepLevel]);
	updatePosition();    // Mise à jour de la position
}


/**
 * Procédure lancée par la méthode toogleOutput.
 * Elle n'est utilisée que pour commander un driver 
 * de moteur pas-à-pas en deux fils de type A3967.
 */
void DinastroStepper::twoWireA3967Driver ()
{
	int oldStepLevel = _StepLevel;
	if (oldStepLevel == LOW)
	{
		_StepLevel = HIGH;
		updatePosition();    // Pas moteur sur front montant : mise à jour de la position
	}
	if (oldStepLevel == HIGH)
	{
		_StepLevel = LOW;
	}  
	digitalWrite(STEP, _StepLevel); 
}
