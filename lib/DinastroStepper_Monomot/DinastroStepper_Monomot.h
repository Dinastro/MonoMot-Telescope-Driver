
#ifndef DINASTROSTEPPER_H
#define DINASTROSTEPPER_H

#include <stdlib.h>
#if ARDUINO >= 100
#include <Arduino.h>
#else
#include <WProgram.h>
#include <wiring.h>
#endif

	/*
	 * Constantes des rampes et états des moteurs
	 */
	// Le tableau de valeurs successives à appliquer à la sortie CCP1 en mode FULL STEP
	static const unsigned char _CCP1_fullStep_Values[32] = {255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
	// Le tableau de valeurs successives à appliquer à la sortie CCP2 en mode FULL STEP
	static const unsigned char _CCP2_fullStep_Values[32] = {255,255,255,255,255,255,255,255,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,255,255,255,255,255,255,255,255};
	// Le tableau de valeurs successives à appliquer à la sortie CNT1 quelque soit le mode de commande moteur
	static const boolean _CNT1_Values[32] = {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
	// Le tableau de valeurs successives à appliquer à la sortie CNT2 quelque soit le mode de commande moteur
	static const boolean _CNT2_Values[32] = {1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1};

/**
  * class DinastroStepper
  * 
  */

class DinastroStepper
{
public:

	// Constructors/Destructors
	//  


	/**
	 * Empty Constructor
	 */
	DinastroStepper (int stepper_step,
						unsigned long timer_period,
						unsigned long tracking_period,
						unsigned long est_centering_period,
						unsigned long west_centering_period,
						unsigned long pointing_period,
						unsigned long catch_up_period,
						unsigned long accel_period,
						unsigned long accel_step_pss,
						unsigned long accel_step_overcome);

	/**
	 * Empty Destructor
	 */
	virtual ~DinastroStepper ();

	// Static Public attributes
	//  
	/*
	 * Constantes Logiques
	 */
	static const int FULL = 1;										// Mode moteur pas entier
	static const int HALF = 2;										// Mode moteur en demi pas
	static const int HEIGHT = 8;									// Mode moteur en 8éme de pas
	static const int CLOCKWISE = 0;									// Sens de rotation des aiguilles d'une montre (mode par défaut du suivi)
	static const int C_CLOCKWISE = 1;								// Sens de rotation inverse des aiguilles d'une montre
	static const int UP = 1;										// Moteur en cours d'accélération
	static const int DOWN = 2;										// Moteur en cours de ralentissement
	static const int CONSTANT = 3;									// Moteur en vitesse constante
	/*
	 * Attribution des signaux de l'étage de puissance
	 */
	static const int DIR = 8;										// Pour Driver PaP type A3967 - Broche Direction
	static const int STEP = 9;										// Pour Driver PaP type A3967 - Broche Step
	static const int MS1 = 10;										// Pour Driver PaP type A3967 - Broche MS1
	static const int MS2 = 11;										// Pour Driver PaP type A3967 - Broche MS2
	static const int ENABLE = 12;									// Pour Driver PaP type A3967 - Broche Enable
	static const int CCP1 = 9;										// Pour carte Monomot (>2.52Pro) - Broche CCP1 (PWM bobine 1) sur pin UNO IO9
	static const int CCP2 = 10;										// Pour carte Monomot (>2.52Pro) - Broche CCP2 (PWM bobine 2) sur pin UNO IO10
	static const int CNT1 = 14;										// Pour carte Monomot (>2.52Pro) - Broche CNT1 (horloge bobine 1) sur pin UNO A0
	static const int CNT2 = 15;										// Pour carte Monomot (>2.52Pro) - Broche CNT2 (horloge bobine 2) sur pin UNO A1
	/*
	 * (1) Constantes des états logiques de fonctionnement
	 */
	static const unsigned int TRACKING_STATE = 0;					// En vitesse de suivi
	static const unsigned int WEST_CENTERING_STATE = 1;				// En vitesse de centrage vers l'Ouest
	static const unsigned int EST_CENTERING_STATE = 2;				// En vitesse de centrage vers l'Est
	static const unsigned int WEST_POINTING_STATE = 3;				// En vitesse de pointage vers l'Ouest
	static const unsigned int EST_POINTING_STATE = 4;				// En vitesse de pointage vers l'Est
	static const unsigned int WEST_CATCH_UP_STATE = 5;				// En vitesse de rattrapage vers l'Ouest
	static const unsigned int EST_CATCH_UP_STATE = 6;				// En vitesse de rattrapage vers l'Est
	static const unsigned int RETURNING_TO_TRACKING_STATE = 7;		// Dans l'état de retour vers la vitesse de suivi (ralentissement depuis une vitesse plus rapide)
	/*
	 * Constantes des rampes et états des moteurs
	 */
	// Le tableau de valeurs successives à appliquer à la sortie CCP1 en mode FULL STEP
	// const unsigned char _CCP1_fullStep_Values[32] = {255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
	// Le tableau de valeurs successives à appliquer à la sortie CCP2 en mode FULL STEP
	//const unsigned char _CCP2_fullStep_Values[32] = {255,255,255,255,255,255,255,255,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,255,255,255,255,255,255,255,255};
	// Le tableau de valeurs successives à appliquer à la sortie CNT1 quelque soit le mode de commande moteur
	//const boolean _CNT1_Values[32] = {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
	// Le tableau de valeurs successives à appliquer à la sortie CNT2 quelque soit le mode de commande moteur
	//const boolean _CNT2_Values[32] = {1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1};
	
	// Public attributes
	//  



	/**
	 * Initialisation du moteur pas à pas.
	 * 
	 * Initialise les connexions vers l'étage de sortie ;
	 * Initialise les variables globales gérants les paramètres moteurs ;
	 * Place le moteur pas à pas en conditions initiale ;
	 * Active le moteur pas à pas.
	 */
	void initStepper ();


	/**
	 * Activation du moteur pas à pas.
	 * 
	 * Basculement de la sortie ENABLE à l'état bas du circuit de commande moteur ;
	 * Activation du Timer2 (échantillonnage des pas et des rampes).
	 */
	void enableStepper ();


	/**
	 * Désactivation du moteur pas à pas.
	 * 
	 * Basculement de la sortie ENABLE à l'état haut du circuit de commande moteur ;
	 * Désactivation du Timer2 (échantillonnage des pas et des rampes).
	 */
	void disableStepper ();


	/**
	 * Retourne la position en nombre de pas, par rapport à l'initilisation du moteur
	 * (type int).
	 * @return int
	 */
	int getPosition ();


	/**
	 * Initialise à zéro la position du moteur.
	 */
	void resetPosition ();


	/**
	 * Retourne le mode de commande du moteur pas à pas (type int).
	 * 
	 * Le moteur pas à pas peut fonctionner en mode pas entier, demi ou 8eme de pas.
	 * La méthode retourne un int correspondant au mode selon les constantes logiques
	 * suivantes :
	 * FULL = 1			Mode moteur pas entier
	 * HALF = 2			Mode moteur en demi pas
	 * HEIGHT = 8			Mode moteur en 8éme de pas
	 * @return int
	 */
	int getStepperMode ();


	/**
	 * Retourne le sens de rotation du moteur (type int).
	 * 
	 * La méthode retourne un int correspondant au sens selon les constantes logiques
	 * suivantes :
	 * CLOCKWISE = 0		Sens de rotation des aiguilles d'une montre (mode par défaut du
	 * suivi)
	 * C_CLOCKWISE = 1		Sens de rotation inverse des aiguilles d'une montre
	 * @return int
	 */
	int getDirection ();


	/**
	 * Retourne l'état logique du moteur (type int).
	 * 
	 * La méthode retourne un int correspondant à l'état logique de fonctionnement du
	 * moteur selon les constantes logiques suivantes :
	 * TRACKING_STATE = 0		en vitesse de suivi
	 * WEST_CENTERING_STATE = 1		en vitesse de centrage vers l'Ouest
	 * EST_CENTERING_STATE = 2		en vitesse de centrage vers l'Est
	 * WEST_POINTING_STATE = 3		en vitesse de pointage vers l'Ouest
	 * EST_POINTING_STATE = 4		en vitesse de pointage vers l'Est
	 * WEST_CATCH_UP_STATE = 5		en vitesse de rattrapage vers l'Ouest
	 * EST_CATCH_UP_STATE = 6		en vitesse de rattrapage vers l'Est
	 * RETURNING_TO_TRACKING_STATE = 7	dans l'état de retour vers la vitesse de suivi
	 * (ralentissement depuis une vitesse plus rapide)
	 * @return int
	 */
	int getState ();


	/**
	 * Lancement de la vitesse de suivi.
	 */
	void launchTracking ();


	/**
	 * Lancement de la vitesse de centrage vers L'Ouest.
	 */
	void launchWestCentering ();


	/**
	 * Lancement de la vitesse de centrage vers l'Est.
	 */
	void launchEstCentering ();


	/**
	 * Lancement de la vitesse de pointage vers l'Est.
	 */
	void launchEstPointing ();


	/**
	 * Lancement de la vitesse de pointage vers l'Ouest.
	 */
	void launchWestPointing ();


	/**
	 * Lancement de la vitesse de rattrapage vers l'Ouest.
	 */
	void launchWestCatchUP ();


	/**
	 * Lancement de la vitesse de rattrapage vers l'Est.
	 */
	void launchEstCatchUP ();


	/**
	 * Lancement de la procédure de retour à la vitesse de suivi après une rotation
	 * rapide.
	 * 
	 * Lance une rampe de ralentissement.
	 */
	void launchReturnToTracking ();
	
	/**
	 * Procédure lancée par le Timer2 (ISR : Interrupt Service Routine).
	 * Basculement des sorties de commande de la broche STEP de l'étage de puissance et
	 * lancement des procédures périodiques.
	 * 
	 * Boucle synchrone en mode interruption sur Timer2.
	 * Toutes les TIMER_PERIOD micro-secondes on parcours cette fonction.
	 * Selon la vitesse de rotation voulue on compare le
	 * _CurrentSteppingPeriodicCounter à une valeur de frequence constante.
	 * On procède de mme pour le calcul de la vitesse en accélération ou
	 * ralentissement.
	 */
	void toogleOutput ();

protected:

	// Static Protected attributes
	//  

	// Protected attributes
	//  

public:

protected:

public:

protected:


private:

	// Static Private attributes
	//  

	// Private attributes
	//  

	// Déclaration Globales privées
	// ( Le mode de fonctionnement du moteur pas à pas (FULL, HALF, HEIGHT))
	int _StepperMode;
	// La position en nombre de pas par rapport à l'initialisation
	int _Pos;
	// Le sens de rotation du moteur (CLOCKWISE, C_CLOCKWISE)
	int _Dir;
	// Vrai si le moteur est activé
	int _StepperEnabled;
	// Le sens de la rampe (UP, DOWN, CONSTANT)
	boolean _AccelSens;
	// Etat logique de fonctionnement courant du moteur (cf plus haut pour valeurs constantes possibles (1))
	unsigned int _State;
	// Le niveau logique du pas moteur courant ou l'identification de l'état courant dans le cycle d'un pas moteur
	int _StepLevel;
	// La valeur courante du compteur de période de changement des pas
	unsigned long _CurrentSteppingPeriodicCounter;
	// Consigne courante du compteur de période
	unsigned long _SteppingPeriodicCounterOrder;
	// La valeur courante du compteur de période de changement des rampes
	unsigned long _CurrentAccelCounter;
	// Consigne courante du compteur de rampes
	unsigned long _AccelCounterOrder;
	// Tableau des différentes périodes de rotation (calculées à partir des constantes moteur)
	unsigned long _StepperReferencePeriod[10];
	// Période d'échantillonage de la mise à jour de la période de rotation lors des phases d'accelération ou de ralentissement
	unsigned long _AccelReferencePeriod;
	// Valeur maximal du compteur de rampes
	unsigned long _MaxAccelSpeedPeriodicCounter;
	// Valeur minimale du compteur de rampes
	unsigned long _MinAccelSpeedPeriodicCounter;
	// Coefficient de rampe issu du coefficient en pas par s-2
	unsigned long _AccelTimerCounterCoef;
	// Un coefficient d'amortissement de la rampe lorsque on approche de la fin
	unsigned long _AccelTimerCounterCoefB;
	// Nombre de cycle avant changement de seuil (coefficient de pallier)
	unsigned long _AccelOvercomeCounter;
	// Nombre de pas lors d'un tour complet du moteur
	int _stepper_step;
	// La période en micro secondes du timer2
	unsigned long _timer_period;
	// La période en micro secondes de la vitesse de suivi
	unsigned long _tracking_period;
	// La période en micro secondes de la vitesse de centrage vers l'Est
	unsigned long _est_centering_period;
	// La période en micro secondes de la vitesse de centrage vers l'Ouest
	unsigned long _west_centering_period;
	// La période en micro secondes de la vitesse de pointage
	unsigned long _pointing_period;
	// La période en micro secondes de la vitesse de rattrapage
	unsigned long _catch_up_period;
	// La période en micro secondes de la boucle de calcul de la vitesse en fonction de l'accelération
	unsigned long _accel_period;
	// L'acceleration en pas par secondes par secondes
	unsigned long _accel_step_pss;
	// La valeur du pallier de maintient de la vitesse contante en phase d'acceleration ou de ralentissement
	unsigned long _accel_step_overcome;

public:

private:

public:

private:



	/**
	 * Mise à jour de la position angulaire (en pas).
	 * 
	 * Met à jour la position en nombre de pas par rapport au point de départ (voir
	 * initStepper()).
	 */
	void updatePosition ();


	/**
	 * Modification du mode de commande du moteur pas à pas.
	 * 
	 * Le moteur pas à pas peut fonctionner en mode pas entier, demi ou 8eme de pas.
	 * La méthode prend un int en paramètre correspondant au mode selon les constantes
	 * logiques suivantes :
	 * FULL = 1			Mode moteur pas entier
	 * HALF = 2			Mode moteur en demi pas
	 * HEIGHT = 8			Mode moteur en 8éme de pas
	 * @param  mode
	 */
	void setStepperMode (int mode);


	/**
	 * Change le sens de rotation du moteur.
	 * 
	 * Modifie la valeur de la sortie DIR de l'étage de sortie.
	 * La méthode prend en paramètre un int correspondant au sens selon les constantes
	 * logiques suivantes :
	 * CLOCKWISE = 0		Sens de rotation des aiguilles d'une montre (mode par défaut du
	 * suivi)
	 * C_CLOCKWISE = 1		Sens de rotation inverse des aiguilles d'une montre
	 * @param  dir
	 */
	void changeDirection (int dir);


	/**
	 * Convertie une période en micro-secondes en nombre de passages dans la procédure
	 * d'échantillonage périodique.
	 * 
	 * Prend un long en paramètre représentant la période en micro secondes.
	 * Retourne un long représentant le nombre de passages dans la procédure
	 * d'échantillonage périodique.
	 * @return long
	 * @param  aFrequency
	 */
	long convertCounterValueFromFrequency (long aFrequency);


	/**
	 * Réalise les calculs d'actialisation de la vitesse de rotation.
	 * 
	 * Assure l'augmentation ou la diminution de la vitesse de rotation selon la
	 * position dans le cycle d'accélération ou de ralentissement.
	 */
	void compute ();
	
	
	/**
	 * Procédure lancée par la méthode toogleOutput.
	 * Elle n'est utilisée que pour commander un driver 
	 * de moteur pas-à-pas en deux fils de type A3967.
	 */
	void twoWireA3967Driver ();


	/**
	 * Procédure lancée par la méthode toogleOutput.
	 * Elle n'est utilisée que pour commander un driver 
	 * de moteur pas-à-pas selon la platine Monomot
	 * (version > 2.52Pro)
	 */
	void monomotDriver ();

};

#endif // DINASTROSTEPPER_H
