# DinastroDriv et Dinastro's MonoMot Telescope Driver
Cartes basées sur l'architecture Arduino UNO destinée à commander des axes d'une monture de telescope / shield based on the Arduino UNO architecture for controlling one axis of a telescope mount

## A Propos de la carte MonoMot

La carte MonoMot est un sous projet de DinastroDrive. Son usage est destiné à commande un moteur pas à pas en mode micro-pas installé sur une monture de télescope. 

Le carte est conçue de sorte à favoriser la modularité et l'interchangeabilité. Ainsi elle s'interface via un bus I2C.

Elle est autonome et peux fonctionner seule sans carte principale DinastroDriv.

Voir le Wiki pour plus de détails : [Wiki](https://framagit.org/Dinastro/MonoMot-Telescope-Driver/wikis/home)

## Auteurs et contributeurs
Le projet a été initié au départ par l'association [Dinastro](http://www.dinastro.org) par Jean-Paul GARRIGOS (@jpegfr) et Jean CAQUEL.

<a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/Dataset" property="dct:title" rel="dct:type">Le Wiki et la documentation des projets DinastroDriv-Telescope-Driver et MonoMot-Telescope-Driver</span> de <span xmlns:cc="http://creativecommons.org/ns#" property="cc:attributionName">Dinastro</span> sont mis à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">licence Creative Commons Attribution - Pas d’Utilisation Commerciale 4.0 International</a>.
